# ================================================ #
#     Compute optimised strategy for (2,2)-chain   #
# ================================================ #

def optimised_strategy(n, mul_c=1):
    """
    Algorithm 60: https://sike.org/files/SIDH-spec.pdf
    Shown to be appropriate for (l,l)-chains in 
    https://ia.cr/2023/508
    
    Note: the costs we consider are:
       eval_c: the cost of one isogeny evaluation
       mul_c:  the cost of one element doubling
    """

    eval_c = 1.000
    mul_c  = mul_c

    S = {1:[]}
    C = {1:0 }
    for i in range(2, n+1):
        b, cost = min(((b, C[i-b] + C[b] + b*mul_c + (i-b)*eval_c) for b in range(1,i)), key=lambda t: t[1])
        S[i] = [b] + S[i-b] + S[b]
        C[i] = cost

    return S, C

def strategy(n, mul_c=1):
    S, C = optimised_strategy(n, mul_c=mul_c)
    return S[n]

# simulate a strategy execution
def simulate_isogeny_chain(strategy):
    n = len(strategy) + 1
    # Store chain of isogenies
    # the element i is the vector of points we push through the i-th isogeny
    # a point P is represented by the index n such that P=2^n*generator
    isogeny_chain = []
    doublings = [] #nb of doublings we do at each steps
    images = [] #nb of images we take at each steps

    # Bookkeeping for optimal strategy
    strat_idx = 0
    level = [0]
    ker = 0 # 1*the generator
    kernel_elements = [ker]

    for k in range(n):
        prev = sum(level)
        ker = kernel_elements[-1]
        double = 0

        while prev != (n - 1 - k):
            level.append(strategy[strat_idx])

            # Perform the doublings
            ker += strategy[strat_idx]
            double += strategy[strat_idx]

            # Update kernel elements and bookkeeping variables
            kernel_elements.append(ker)
            prev += strategy[strat_idx]
            strat_idx += 1

        # Update the chain of isogenies
        isogeny_chain.append(list(kernel_elements))
        doublings.append(double)

        # Remove elements from list
        kernel_elements.pop()
        level.pop()

        # push points for next step
        images.append(len(kernel_elements))

    return sum(doublings), sum(images), isogeny_chain, doublings, images

# 2^n isogenies en dim 1
# prenons n=2m pair.
# 0) doublements en 2S+4M+4a pour un doublement
# 1) on décompose en m 4-isogénies
#    quadruplement en 4S+8M+8a
#    images en 2S+6M+6a
#    codomaine en 4S+4a
# 2) on décompose en n-2 2-isogénie + 1 4-isogénie à la fin
#    doublement en 2S+4M+4a
#    images en 2S+2M+3a
#    codomaine en 4S+6a
#    Et pour l'isogénie à la fin:
#    codomaine gratuit, image en 4M+2S+9a

def compare_methods(n, mul_c1=1.0, mul_c2=1.0):
    def show_result(sq, mult, add):
        total=sq+mult
        c1=sq*0.66+mult
        c2=sq*0.66+mult+add*0.01
        print(f"Total: {total} [{sq}S+{mult}M]+{add}a -> {c1} / {c2}")
    m=n//2
    S1=strategy(m, mul_c=mul_c1)
    doublings, images, *_ = simulate_isogeny_chain(S1)
    #print(doublings, images, *_)
    squares=doublings*4+images*2+m*4
    mult=doublings*8+images*6
    additions=doublings*8+images*6+m*4
    r1=[squares,mult,additions]
    show_result(*r1) #codomain computation
    show_result(m*2, m*6, m*6) #images

    m=n-2
    S2=strategy(m, mul_c=mul_c2)
    doublings, images, *_ = simulate_isogeny_chain(S2)
    #print(doublings, images, *_)
    squares=doublings*2+images*2+m*4
    mult=doublings*4+images*2
    additions=doublings*4+images*3+m*6
    r2=[squares,mult,additions]
    show_result(*r2)
    show_result(m*2+2, m*2+4, m*3+9)

    return r1,r2

# compare_methods(128)
# Total: 768 [416S+352M]+608a -> 626.560000000000 / 632.640000000000
# Total: 760 [600S+160M]+932a -> 556.000000000000 / 565.320000000000

