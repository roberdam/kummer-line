def double(A24, P):
    X, Z = P
    XpZ=X+Z
    XmZ=X-Z
    a=(XpZ)*(XpZ) #in Sage this is faster than XpZ**2
    b=(XmZ)*(XmZ)
    c=a-b
    X2=a*b
    Z2=c*(b+A24*c)
    return (X2, Z2)

# this is the correct differential addition from the cubical torsor point of view
def cubical_diff_add(P, Q, PmQ):
    XP, ZP = P
    XQ, ZQ = Q
    XPmQ, ZPmQ = PmQ
    a=XP+ZP
    b=XP-ZP
    c=XQ+ZQ
    d=XQ-ZQ
    da = d*a
    cb = c*b
    dapcb = da+cb
    damcb = da-cb
    XPQ=dapcb*dapcb / XPmQ
    ZPQ=damcb*damcb / ZPmQ
    return (XPQ/4, ZPQ/4)

# this is the differential addition usually implemented in the Montgomery ladder for cryptography
def diff_add(P, Q, PmQ):
    XP, ZP = P
    XQ, ZQ = Q
    XPmQ, ZPmQ = PmQ
    a=XP+ZP
    b=XP-ZP
    c=XQ+ZQ
    d=XQ-ZQ
    da = d*a
    cb = c*b
    dapcb = da+cb
    damcb = da-cb
    XPQ=dapcb*dapcb * ZPmQ
    ZPQ=damcb*damcb * XPmQ
    return (XPQ, ZPQ)

# the cubical ladder will be used to compute our canonical lift
def cubical_ladder(A24, P, n):
    n = abs(n)
    P1, P2 = (1, 0), P
    if n == 0:
        return P1, P2
    for bit in bin(n)[2:]:
        Q = cubical_diff_add(P2, P1, P)
        if bit == "1":
            P2 = double(A24, P2)
            P1 = Q
        else:
            P1 = double(A24, P1)
            P2 = Q
    return P1

# the standard ladder will be used to simulate a projective coordinate leak recovery
def ladder(A24, P, n):
    n = abs(n)
    P1, P2 = (1, 0), P
    if n == 0:
        return P1, P2
    for bit in bin(n)[2:]:
        Q = diff_add(P2, P1, P)
        if bit == "1":
            P2 = double(A24, P2)
            P1 = Q
        else:
            P1 = double(A24, P1)
            P2 = Q
    return P1

def ratio(P, Q):
    XP, ZP = P
    XQ, ZQ = Q
    if XP == 0:
        assert XQ == 0
        return (ZQ/ZP)
    else:
        l=XQ/XP
        assert (ZQ == l*ZP)
        return l

if __name__ == "__main__" and "__file__" in globals():
    from sage.all import ZZ, GF, EllipticCurve, CRT, Mod, PolynomialRing, Zmod

    p=2039; F=GF(p)
    AC=5; E=EllipticCurve(GF(p), [0,AC,0,1,0]) # #E=2^2*251
    cofactor = 2**2; l=251
    P = cofactor * E.random_point()
    assert P != E(0)

    # projective coordinate leak via the cubical ladder
    def cubical_dlp(P, m):
        l=P.order()
        E=P.curve()
        ainvs = E.a_invariants()
        A = ainvs[1]
        assert ainvs == (0, A, 0, 1, 0)
        A24=(A+2)/4
        xP=P[0]; F=xP.parent(); p=F.cardinality()
        zP=P[2]; assert zP==1
        P=(xP, zP)

        u = CRT(0,1, p-1, l)
        Q=cubical_ladder(A24, P,m)
        Ptilde=cubical_ladder(A24, P, u) # "Canonical lift"
        Qtilde=cubical_ladder(A24, Q, u)
        l2 = ratio(Qtilde, Q)
        l1 = ratio(Ptilde, P)
        assert l2 == l1**(m**2)

    cubical_dlp(P,5)
    cubical_dlp(P,11)
    cubical_dlp(P,59)

    # projective coordinate leak via the standard ladder
    # For this attack to work, the attacker needs to know:
    # 1) P=(X_P,Z_P) and the binary length of m
    # 2) Q=(X_Q,Z_Q) as computed by the Montgomery ladder
    def dlp(P, m):
        l=P.order()
        E=P.curve()
        ainvs = E.a_invariants()
        A = ainvs[1]
        assert ainvs == (0, A, 0, 1, 0)
        A24=(A+2)/4
        xP=P[0]; F=xP.parent(); p=F.cardinality()
        zP=P[2]; assert zP==1
        P=(xP, zP)

        u = CRT(0,1, p-1, l)
        Q=ladder(A24, P,m)
        Ptilde=cubical_ladder(A24, P, u) # "Canonical lift"
        Qtilde=cubical_ladder(A24, Q, u)
        l2 = ratio(Qtilde, Q)
        l1 = ratio(Ptilde, P)

        zeta=F.multiplicative_generator()
        dlp_x=Mod((4*xP).log(zeta),p-1)
        dlp_l2=Mod(l2.log(zeta),p-1)
        dlp_l1=Mod(l1.log(zeta),p-1)
        l=len(bin(m))-2

        # dlp_x*m*(2**l-m) is the corrective projective factor we need to
        # use because we used the standard ladder to compute Q instead of
        # the "correct" cubical ladder
        assert dlp_x*m*(2**l-m)+dlp_l1*m**2 == dlp_l2
        # NB: the assert is just here to check that our correction is
        # indeed the correct one; this is not used to recover 'm' below

        R=PolynomialRing(Zmod(p-1), name="X")
        X=R.gen()
        P=X**2*(dlp_l1-dlp_x)+2**l*dlp_x*X - dlp_l2
        # we need to find the roots of this polynomial
        # here we cheat a bit and use the fact that we selected p so that
        # p-1=2q, q prime
        Fq=GF((p-1)/2)
        Pq=P.change_ring(Fq)
        return Pq.roots()

    # Here the roots include m
    print(dlp(P,5))
    print(dlp(P,11))
    print(dlp(P,138))
    print(dlp(P,239))
    print(dlp(P,313))
    # Note thet 2517 is greater than (p-1)/2, so we are starting to wrap
    # around in our monodromy, but we get the correct value mod (p-1)/2
    print(dlp(P,2517)) #NB: 479=2517 mod (p-1)/2
