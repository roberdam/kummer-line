# 2S+2M+1m0+4a
# a=(X+Z)^2, b=(X-Z)^2, c=4XZ, X2=ab, Z2=c(b+A24c)
def double(A24, P):
    X, Z = P
    XpZ=X+Z
    XmZ=X-Z
    a=(XpZ)*(XpZ) #in Sage this is faster than XpZ**2
    b=(XmZ)*(XmZ)
    c=a-b
    X2=a*b
    Z2=c*(b+A24*c)
    return (X2, Z2)

# 2S+3M+6a
# X(P+Q)X(P-Q)=[(XP+ZP)(XQ-ZQ)+(XP-ZP)(XQ+ZQ)]^2
# Z(P+Q)Z(P-Q)=[(XP+ZP)(XQ-ZQ)-(XP-ZP)(XQ+ZQ)]^2
def diff_add(P, Q, ixPmQ, correct=False):
    XP, ZP = P
    XQ, ZQ = Q
    a=XP+ZP
    b=XP-ZP
    c=XQ+ZQ
    d=XQ-ZQ
    da = d*a
    cb = c*b
    dapcb = da+cb
    damcb = da-cb
    XPQ=dapcb*dapcb * ixPmQ
    ZPQ=damcb*damcb
    if correct:
        return (XPQ/4, ZPQ/4)
    # Note: one can use the formula below, this gains the division by 4 for each diff add. The only caveat is that the Tate pairing with embedding degree 1 will not be correct anymore
    else:
        return (XPQ, ZPQ)

# This assume we have the cubical points P1, P2, Q, P1+Q, P2+Q
# and this returns P1+P2 (arbitrary via compatible addition), P1+P2+Q (via three way add)
def add(A24, P1, P2, Q, P1Q, P2Q):
    # operation count: knowing that P1, P1Q, Q are normalised to have Z=1
    XP1, ZP1=P1
    XP2, ZP2=P2
    XQ, ZQ = Q
    XP1Q, ZP1Q=P1Q
    XP2Q, ZP2Q=P2Q
    A=4*A24-2

    κ00=(XP1*XP2-ZP1*ZP2)**2 #1M+1S
    κ11=(XP1*ZP2-XP2*ZP1)**2 #1M+1S
    κ01=2*((XP1*XP2+ZP1*ZP2)*(XP1*ZP2+XP2*ZP1)+2*A*XP1*XP2*ZP1*ZP2) #1M+1M+1M0
    # Total: 4M+2S+1M0

    κ00b=(XP1Q*XP2Q-ZP1Q*ZP2Q)**2
    κ11b=(XP1Q*ZP2Q-XP2Q*ZP1Q)**2
    κ01b=2*((XP1Q*XP2Q+ZP1Q*ZP2Q)*(XP1Q*ZP2Q+XP2Q*ZP1Q)+2*A*XP1Q*XP2Q*ZP1Q*ZP2Q)
    # Total: 4M+2S+1M0

    XP1mP2=κ01b*κ00-κ01*κ00b #2M
    ZP1mP2=κ11b*κ00-κ11*κ00b #2M
    # Total: 4M

    nZP1P2Q=(XP2*ZP1Q-XP1Q*ZP2)*(XP1*ZP2Q-XP2Q*ZP1) #3M
    dZP1P2Q=(XP1mP2*ZQ-ZP1mP2*XQ) #1M

    nXP1P2Q=(XP2*XP1Q-ZP1Q*ZP2)*(XP1*XP2Q-ZP2Q*ZP1) #4M
    dXP1P2Q=(XP1mP2*XQ-ZP1mP2*ZQ) #2M
    # Total: 10M

    # XP1P2=κ00/XP1mP2
    # ZP1P2=κ11/ZP1mP2
    # return ((XP1P2, ZP1P2), (nXP1P2Q/dXP1P2Q, nZP1P2Q/dZP1P2Q))
    # We will use the Gm action to get rid of divisions

    # 2M for dXP1P2Q*dZP1P2Q and XP1mP2*ZP1mP2
    XP1P2=κ00*ZP1mP2 * dXP1P2Q*dZP1P2Q # 2M
    ZP1P2=κ11*XP1mP2 * dXP1P2Q*dZP1P2Q # 2M
    XP1P2Q=nXP1P2Q*dZP1P2Q * XP1mP2*ZP1mP2 # 2M
    ZP1P2Q=nZP1P2Q*dXP1P2Q * XP1mP2*ZP1mP2 # 2M
    # Total: 10M
    # Grand total: (4M+2S+1M0)*2 + 4M + 10M + 10M = 32M + 4S + 2M0

    return ((XP1P2, ZP1P2), (XP1P2Q, ZP1P2Q))

# like add, except we suppose we start with P-Q rather than P+Q
# and we only return (n+1)P
# this will be used for a Double / DoubleAndAdd algorithm
# So we have P1, P2, P1-Q, P2+Q, and we return P1+P2
def add_bis(A24, P1, P2, P1mQ, P2Q):
    # operation count: knowing that P1, P1mQ, Q are normalised to have Z=1
    XP1, ZP1=P1
    XP2, ZP2=P2
    XP1mQ, ZP1mQ=P1mQ
    XP2Q, ZP2Q=P2Q
    A=4*A24-2

    # P1+P2
    κ00=(XP1*XP2-ZP1*ZP2)**2 #1M+1S
    κ11=(XP1*ZP2-XP2*ZP1)**2 #1M+1S
    κ01=2*((XP1*XP2+ZP1*ZP2)*(XP1*ZP2+XP2*ZP1)+2*A*XP1*XP2*ZP1*ZP2) #1M+1M+1M0
    # Total: 4M+2S+1M0

    # P1-Q + P2+Q
    κ00b=(XP1mQ*XP2Q-ZP1mQ*ZP2Q)**2
    κ11b=(XP1mQ*ZP2Q-XP2Q*ZP1mQ)**2
    κ01b=2*((XP1mQ*XP2Q+ZP1mQ*ZP2Q)*(XP1mQ*ZP2Q+XP2Q*ZP1mQ)+2*A*XP1mQ*XP2Q*ZP1mQ*ZP2Q)
    # Total: 4M+2S+1M0

    XP1P2=κ01b*κ00-κ01*κ00b #2M
    ZP1P2=κ11b*κ00-κ11*κ00b #2M
    # Total: 4M

    return (XP1P2, ZP1P2) # -> 12M+4S+2M0 to get (n+1)P

# returns nQ, P+nQ,
# 6S+8M+1m0+16a by bit (-4a if we inlined everything)
#
# NB: if Q in Fp and P in Fpk, this costs
# 4S+5M+1m0 + 2SS+1MM+2Mm
# if P in Fp[i], then 1SS=2M, 1MM=3M, 1Mm=2M, and the cost becomes 4S+17M
#
# By comparison, the best generic Miller algorithm over Fq seems to be in https://eprint.iacr.org/2016/963.pdf $4.1 where a doubling costs 14M+7S in (X^2: XZ: Z^2: YZ) coordinates [15M+8S in (X:Y:Z) coordinates] and an addition costs 20M+3S in (X:Y:Z) coordinates
# So with S=0.66M, one ladder step is 0.7 Miller doubling, or 0.44 (Miller doubling+1/2 Miller addition)
# so we can expect between x1.4 and x2 speed up
#
# TODO: the ladder3 will fail if P, Q or P+Q is equal to 0=(1:0) or T=(0:1), because the diff_add assumes that both coordinates of the difference are non zero.
# We could handle this as follow (taking into account that we work in the biextension so need affine differential additions):
# if P-Q = 0, diff_add(P~, Q~, 0~)=λ double(P~) where Q~=λ1 P~ and 0~ = λ2 0 and λ=λ1^2/λ2
# if P-Q = T, diff_add(P~, Q~, T~)=λ double(P~)+T~ where Q~=λ1 P~ and T~ = λ2 T.
# But this is probably better handled globally at the level of the pairing
# computations, after all a pairing with 0 or T is trivial to compute.
# For now we skip this case.
# Apart from this, unlike Miller's algorithm, there are no exceptional case during the execution of the ladder3 algorithm
# Output: nQ, P+nQ
def ladder3(n, A24, P, Q, PQ, ixP, ixQ, ixPQ, correct=False):
    nQ=Q
    nQQ=double(A24, Q)
    PnQQ=diff_add(PQ, Q, ixP, correct=correct)

    if n==2:
        return (nQQ, PnQQ)

    # Montgomery-ladder
    for bit in bin(n-1)[3:]:
        R = diff_add(nQQ, nQ, ixQ, correct=correct)
        if bit == "0":
            PnQQ=diff_add(PnQQ, nQ, ixPQ, correct=correct)
            nQ=double(A24, nQ)
            nQQ=R
        else:
            PnQQ=diff_add(PnQQ, nQQ, ixP, correct=correct)
            nQQ=double(A24, nQQ)
            nQ=R
    return (nQQ, PnQQ) #9M+6S by bit

# nb: in the double and add ladder, 1/x(Q) and 1/x(P+Q) are not used
# Output: nQ, P+nQ
def doubleadd_ladder(n, A24, P, Q, PQ, ixP, ixQ, ixPQ, correct=False):
    nQ=Q; PnQ=PQ
    for bit in bin(n)[3:]:
        #6M+4S for a double
        PnQ=diff_add(PnQ, nQ, ixP, correct=correct)
        nQ=double(A24, nQ)
        if bit == "1":
            #34M+4S for an add
            nQ, PnQ = add(A24, Q, nQ, P, PQ, PnQ)
    return (nQ, PnQ)

# nb: in the double / double and add ladder, we need to start with Q-P rather than P+Q (so if we provide P+Q we will get the pairing e(-P,Q) instead)
def doubledoubleadd_ladder(n, A24, P, Q, QmP, ixP, ixQ, ixQmP, correct=False):
    nQ=Q; PnQ=diff_add(Q,P,ixQmP)
    for bit in bin(n)[3:]:
        if bit == "0":
            # 2x(3M+2S)=6M+4S for a double
            PnQ=diff_add(PnQ, nQ, ixP, correct=correct)
            nQ=double(A24, nQ)
        if bit == "1":
            # Get (n+1)Q from Q, nQ, Q-P, P+nQ
            n1Q = add_bis(A24, Q, nQ, QmP, PnQ) #14M+4S
            # P+(2n+1)Q
            PnQ=diff_add(n1Q, PnQ, ixQmP, correct=correct)
            # (2n+1)Q
            nQ=diff_add(n1Q, nQ, ixQ, correct=correct) #+2x(3M+2S)=6M+4S
            # Total: 20M+8S for a double and add
    return (nQ, PnQ)

# if n=2^m, we only need biextension doublings...
def fast_twotothem_ladder(m, A24, P, Q, PQ, ixP, correct=False):
    nQ=Q
    PnQ=PQ
    if m==0:
        return (nQ, PnQ)
    # Doublings in the biextension
    for i in range(0, m):
        PnQ=diff_add(PnQ, nQ, ixP, correct=correct)
        nQ=double(A24, nQ)
    return (nQ, PnQ)

# for a self pairing e(Q,Q) we only need a standard scalar mult
def scalar_mult(n, A24, Q, ixQ, correct=False):
    nQ=Q
    nQQ=double(A24, Q)

    if n==2:
        return nQQ

    # Montgomery-ladder
    for bit in bin(n-1)[3:]:
        R = diff_add(nQQ, nQ, ixQ, correct=correct)
        if bit == "0":
            nQ=double(A24, nQ)
            nQQ=R
        else:
            nQQ=double(A24, nQQ)
            nQ=R
    return nQQ

# Exponentiation in the biextension.
# We represent g_P,Q via [0~=(1,0), P~; Q~, P+Q~]
# Sage already gives as input normalised points, so we just need to
# normalise the inverted coordinates. If we were given projective points P,
# Q, P+Q, the normalisation process would take 1I + 15M + 6M = 1I+21M
#
# TODO: we assume for now that xP, xQ, xPQ !=0, see above
#
# Remark: if we don't want to use an inversion to normalise our points, we
# need to be careful in the way we set up things so that ladder3 still
# compute the biextension exponentiation. This is more difficult for an
# even degree pairing, because we want to descend back to level 1, so we
# need to start with a biextension element g_P,Q in level 2 which is the
# tensorial square of a biextension element h_P,Q in level 1. Normalising
# everything to have Z=1 saves a lot of headache, and also 2M by bits in
# the ladder, all for the cost of one global inversion.
def biext_ladder(n, A24, xP, xQ, xPQ, ladder=ladder3, correct=False):
    # batch inversion: 1I+6M
    xPxQ = xP*xQ
    xPxQxPQ = xPxQ*xPQ
    ixPxQxPQ = 1/xPxQxPQ
    ixPQ = xPxQ * ixPxQxPQ
    ixPxQ = xPQ * ixPxQxPQ
    ixP = xQ * ixPxQ
    ixQ = xP * ixPxQ
    P = (xP, 1)
    Q = (xQ, 1)
    PQ = (xPQ, 1)
    # the normalisation costs one global inversion, but saves 2M by bits in
    # the ladder, so is worth it for n large enough
    nQ, PnQ = ladder(n, A24, P, Q, PQ, ixP, ixQ, ixPQ, correct=correct)
    return (P, nQ, PnQ)

# Miller via a double and add biextension ladder
def biext_doubleadd(*a, **kws):
    return biext_ladder(*a, **kws, ladder=doubleadd_ladder)
def biext_doubledoubleadd(*a, **kws):
    return biext_ladder(*a, **kws, ladder=doubledoubleadd_ladder)

def fast_twotothem_biext_ladder(m, A24, xP, xQ, xPQ, correct=False):
    ixP = 1/xP
    P = (xP, 1)
    Q = (xQ, 1)
    PQ = (xPQ, 1)
    nQ, PnQ = fast_twotothem_ladder(m, A24, P, Q, PQ, ixP, correct=correct)
    return (P, nQ, PnQ)

def biext_mult(m, A24, xQ, correct=False):
    ixQ = 1/xQ
    Q = (xQ, 1)
    nQ = scalar_mult(m, A24, Q, ixQ, correct=correct)
    return (Q, nQ)

# Affine translation by a two torsion point
# needed for even degree Tate pairings
def translate(P, T):
    A, B=T
    x, z=P
    if B == 0: # T = (1:0) is the neutral point
        return P
    elif A == 0: # T = (0:1)
        return (z,x)
    else:
        return (A*x-B*z, B*x-A*z)

# Give the ratio of two biextension elements g1_P,Q; g2_P,Q
# we compute the ratio of the X coordinates, because we know that the
# neutral point has X != 0.
# TODO: this fails if one of our point is the 2-torsion point (0:1); it would be easy to correct this by looking at the Z coordinate, but by the above discussion we skip this case anyway
def ratio(P1, P2, P1bis, P2bis):
    xP1, zP1 = P1
    xP2, zP2 = P2
    xP1b, zP1b = P1bis
    xP2b, zP2b = P2bis
    ## sanity checks
    # assert zP1b == zP1 * xP1b/xP1
    # assert zP2b == zP2 * xP2b/xP2
    return (xP1 * xP2b / (xP1b * xP2))

# Miller via a cubical ladder
def miller(n, A24, xP, xQ, xPQ, correct=False):
    P, nQ, PnQ = biext_ladder(n, A24, xP, xQ, xPQ, correct=correct)
    return PnQ[1]/nQ[1]

def non_reduced_tate(n, A24, xP, xQ, xPQ, ladder=biext_ladder, correct=False):
    if n%2 == 0:
        # special case for an even degree Tate pairing
        # since we work with the biextension associated to 2(0_E), we
        # compute the square of the usual Tate and Weil pairings. This is
        # not really a problem when n is odd, but when n is even we lose
        # one bit of information. But in that case we can use the action of
        # the theta group G(2(0_E)) to descend back to level 1 and
        # compute the Tate and Weil pairing associated to (0_E).
        # But we need to be careful about the normalisation we use for our
        # starting element g_P,Q in the biextrension, see the discussion
        # above.
        P, nQ, PnQ = ladder(n//2, A24, xP, xQ, xPQ, correct=correct)
        PnQ = translate(PnQ, nQ)
        nQ = translate(nQ, nQ)
    else:
        P, nQ, PnQ = ladder(n, A24, xP, xQ, xPQ, correct=correct)
    neutral = (1, 0) #we assume that in AC=(A,C), the constants has been normalised so that C=1; otherwise for the biextension arithmetic, the correct lift of the neutral point should be (C,0)
    return ratio(neutral, P, nQ, PnQ)

# alternative: rather than computing nQ, P+nQ and comparing with 0, P
# we compute (n-1)Q, P+(n-1)Q and compare with -Q, P-Q
# we could also compute (n+1)Q, P+(n+1)Q and compare with Q, P+Q
def non_reduced_tate_alt(n, A24, xP, xQ, xPQ, ladder=biext_ladder, correct=False):
    P = (xP, 1)
    Q = (xQ, 1)
    #PQ = (xPQ, 1)
    PmQ = diff_add(P, Q, 1/xPQ, correct=correct) #todo: we compute this inverse in 'ladder'
    P, nQ, PnQ = ladder(n-1, A24, xP, xQ, xPQ, correct=correct)
    return ratio(Q, PmQ, nQ, PnQ)

def fast_twotothem_non_reduced_tate(m, A24, xP, xQ, xPQ, correct=False):
    P, nQ, PnQ = fast_twotothem_biext_ladder(m-1, A24, xP, xQ, xPQ, correct=correct)
    PnQ = translate(PnQ, nQ)
    nQ = translate(nQ, nQ)
    neutral = (1, 0)
    return ratio(neutral, P, nQ, PnQ)

def non_reduced_self_tate(m, A24, xQ, correct=False):
    Q, nQ = biext_mult(m, A24, xQ, correct=correct)
    return nQ[0]/1


def tate(n, A24, xP, xQ, xPQ, k=1, nr_tate=non_reduced_tate, ladder=biext_ladder, correct=False):
    p=xP.base_ring().characteristic()
    d=(p**k-1)/n
    e = nr_tate(n, A24, xP, xQ, xPQ, ladder=ladder, correct=correct)
    if k==1:
        # in diff_add, we do not use the correct cubical formula
        # return (XPQ/4, ZPQ/4)
        # but we use (XPQ, ZPQ), so we are off by a factor 4.
        # For the Tate pairing with k=1 we need to correct this.
        F=xP.parent()
        l=len(bin(n))-2
        e=e/F(4)**(n*2**l-n) #this adjusts for nQ
        #TODO: still need to adjust for nQ+P
    return e**d

def tate_doubleadd(*a, **kw):
    return tate(*a, ladder=biext_doubleadd, nr_tate=non_reduced_tate_alt, **kw)
def tate_doubledoubleadd(*a, **kw):
    #return tate(*a, ladder=biext_doubledoubleadd, nr_tate=non_reduced_tate_alt, **kw)
    return tate(*a, ladder=biext_doubledoubleadd, **kw)

# the reduced Tate pairing is computed by starting with an element g_P,Q
# above (P,Q) in the biextension and raising it to the power q-1.
# This is what we do in 'tate2' below.
# But, like for the standard Miller algorithm,
# it is faster to raise g_P,Q to the power l to land in Gm, and then do
# the remaining exponentiation by (q-1)/l in Gm.
# This is what the function 'tate' above does.
def tate2(A24, xP, xQ, xPQ, k=1, correct=False):
    p=xP.base_ring().characteristic()
    n=p**k-1
    P, nQ, PnQ = biext_ladder(n, A24, xP, xQ, xPQ, correct=correct)
    # NB: for this 'slow alternative', we need to use neutral=(4,0) if we
    # use the incorrect cubical diff add which is off by a factor 4,
    # but we need to use neutral=(1,0) if we use the correct cubical
    # formula
    if correct:
        neutral = (1, 0)
    else:
        neutral = (4, 0)
    return ratio(neutral, P, nQ, PnQ)

def fast_twotothem_tate(m, A24, xP, xQ, xPQ, k=1):
    p=xP.base_ring().characteristic()
    d=(p**k-1)/(2**m)
    e = fast_twotothem_non_reduced_tate(m, A24, xP, xQ, xPQ)
    return e**d

def self_tate(n, A24, xQ, k=1):
    p=xQ.base_ring().characteristic()
    d=(p**k-1)/n
    e = non_reduced_self_tate(n, A24, xQ)
    return e**d



def weil(n, A24, xP, xQ, xPQ, ladder=biext_ladder):
    e1 = non_reduced_tate(n, A24, xP, xQ, xPQ, ladder=ladder)
    e2 = non_reduced_tate(n, A24, xQ, xP, xPQ, ladder=ladder)
    return e1/e2

def fast_twotothem_weil(m, A24, xP, xQ, xPQ):
    e1 = fast_twotothem_non_reduced_tate(m, A24, xP, xQ, xPQ)
    e2 = fast_twotothem_non_reduced_tate(m, A24, xQ, xP, xPQ)
    return e1/e2

if __name__ == "__main__" and "__file__" in globals():
    import time
    from sage.all import ZZ, GF, EllipticCurve, proof

    import cypari2
    pari = cypari2.Pari()
    show_results = False
    #show_results = True

    def weil_pairing_pari(P, Q, n):
        return pari.ellweilpairing(P.curve(), P, Q, n)

    def tate_pairing_pari(P, Q, n, k=1):
        E = P.curve()
        p=P[0].base_ring().characteristic()
        d=(p**k-1)/n
        e=pari.elltatepairing(E, P, Q, n)
        return e**d

    def test_odd_degree_pairings(E0, n, cofactor, A24, k=1, points=None):
        if points:
            P,Q=points
        else:
            P = E0.random_point()
            Q = cofactor * E0.random_point()

        PQ=P+Q
        xP=(P[0]/P[2])
        xQ=(Q[0]/Q[2])
        xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.tate_pairing(P, n, k=k)
        print(f"Standard Tate pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=tate_pairing_pari(Q, P, n, k=k)
        print(f"Pari's Tate pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=tate(n, A24, xP, xQ, xPQ, k=k)
        print(f"Biextension Tate pairing computation: {time.time() - time0:.5f}")
        assert t1**2 == t2

        time0 = time.time()
        t2bis=tate2(A24, xP, xQ, xPQ, k=k)
        print(f"Biextension Tate pairing computation (slow alternative): {time.time() - time0:.5f}")
        assert(t1**2 == t2bis)

        time0 = time.time()
        t2ter=tate_doubleadd(n, A24, xP, xQ, xPQ, k=k)
        #t2ter=tate(n, A24, xP, xQ, xPQ, k=k, ladder=biext_ladder, nr_tate=non_reduced_tate_alt)
        print(f"Biextension Tate pairing computation (double and add): {time.time() - time0:.5f}")
        assert(t1**2 == t2ter)

        time0 = time.time()
        t2ter2=tate_doubledoubleadd(n, A24, xP, xQ, xPQ, k=k)
        print(f"Biextension Tate pairing computation (double and double add): {time.time() - time0:.5f}")
        assert(t1**2 == 1/t2ter2) #the sign is because doubledoubleadd expect Q-P rather than P+Q

        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

        if points:
            P,Q=points
        else:
            P=cofactor*P
            PQ=P+Q
            xP=(P[0]/P[2])
            xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.weil_pairing(P, n)
        print(f"Standard Weil pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=weil_pairing_pari(Q, P, n)
        print(f"Pari's Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=weil(n, A24, xP, xQ, xPQ)
        print(f"Biextension Weil pairing computation: {time.time() - time0:.5f}")
        assert t1**2 == t2

        time0 = time.time()
        t2bis=weil(n, A24, xP, xQ, xPQ, ladder=biext_doubleadd)
        print(f"Biextension Weil pairing computation (double and add): {time.time() - time0:.5f}")
        assert t1**2 == t2bis

        time0 = time.time()
        t2bis2=weil(n, A24, xP, xQ, xPQ, ladder=biext_doubledoubleadd)
        print(f"Biextension Weil pairing computation (double and doubleadd): {time.time() - time0:.5f}")
        assert t1**2 == 1/t2bis2

        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

    def test_self_pairings(E0, n, cofactor, A24, k=1):
        Q = E0.random_point()
        Q = cofactor * E0.random_point()
        QQ=Q+Q

        xQ=(Q[0]/Q[2])
        xQQ=(QQ[0]/QQ[2])

        time0 = time.time()
        t1=Q.tate_pairing(Q, n, k=k)
        print(f"Standard Tate self-pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=tate_pairing_pari(Q, Q, n, k=k)
        print(f"Pari's Tate self-pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=tate(n, A24, xQ, xQ, xQQ, k=k)
        print(f"Biextension Tate self-pairing computation: {time.time() - time0:.5f}")
        assert t1**2 == t2

        time0 = time.time()
        t2bis=self_tate(n, A24, xQ, k=k)
        print(f"Biextension fast Tate self-pairing computation: {time.time() - time0:.5f}")
        assert t1**2 == t2

        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

    def test_even_degree_pairings(E0, n, cofactor, A24, k=1, P=None, Q=None, correct=False):
        if P is None:
            P = E0.random_point()
        if Q is None:
            Q = cofactor * E0.random_point()
        # # find a point of exact order n
        # while ((n//2)*Q == E0(0)):
        #     Q = cofactor * E0.random_point()

        PQ=P+Q
        xP=(P[0]/P[2])
        xQ=(Q[0]/Q[2])
        xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.tate_pairing(P, n, k=k)
        print(f"Standard Tate pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=tate_pairing_pari(Q, P, n, k=k)
        print(f"Pari's Tate pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=tate(n, A24, xP, xQ, xPQ, k=k, correct=correct)
        print(f"Biextension Tate pairing computation: {time.time() - time0:.5f}")
        print(t1)
        print(t2)
        print(t1/t2)
        assert t1 == t2

        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

        P=cofactor*P
        #while ((n//2)*P == E0(0)):
        #    P = cofactor * E0.random_point()
        PQ=P+Q
        xP=(P[0]/P[2])
        xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.weil_pairing(P, n)
        print(f"Standard Weil pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=weil_pairing_pari(Q, P, n)
        print(f"Pari's Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=weil(n, A24, xP, xQ, xPQ)
        print(f"Biextension Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t2
        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

    def test_twotothem_degree_pairings(E0, n, cofactor, A24, k=1):
        m = n.log(2)
        P = E0.random_point()
        Q = cofactor * E0.random_point()

        PQ=P+Q
        xP=(P[0]/P[2])
        xQ=(Q[0]/Q[2])
        xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.tate_pairing(P, n, k=k)
        print(f"Standard Tate pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=tate_pairing_pari(Q, P, n, k=k)
        print(f"Pari's Tate pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=tate(n, A24, xP, xQ, xPQ, k=k)
        print(f"Biextension Tate pairing computation: {time.time() - time0:.5f}")
        assert t1 == t2

        time0 = time.time()
        t2bis=fast_twotothem_tate(m, A24, xP, xQ, xPQ, k=k)
        print(f"Fast 2^m biextension Tate pairing computation: {time.time() - time0:.5f}")
        assert t1 == t2bis
        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

        P=cofactor*P
        PQ=P+Q
        xP=(P[0]/P[2])
        xPQ=(PQ[0]/PQ[2])

        time0 = time.time()
        t1=Q.weil_pairing(P, n)
        print(f"Standard Weil pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1bis=weil_pairing_pari(Q, P, n)
        print(f"Pari's Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t1bis

        time0 = time.time()
        t2=weil(n, A24, xP, xQ, xPQ)
        print(f"Biextension Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t2

        time0 = time.time()
        t2bis=fast_twotothem_weil(m, A24, xP, xQ, xPQ)
        print(f"Fast 2^m biextension Weil pairing computation: {time.time() - time0:.5f}")
        assert t1 == t2bis
        if show_results:
            print(f"Result={t1}\n")
        else:
            print()

    proof.all(False)

    def test1():
        ea=74
        eb=41
        A = ZZ(2**ea)
        B = ZZ(3**eb)
        p = A * B - 1
        F = GF(p**2, name="i", modulus=[1, 0, 1])
        i = F.gen()

        #Estart = EllipticCurve(F, [1, 0])
        #T = 2**(ea-8)*B*Estart.random_point()
        #phistart = Estart.isogeny(T)
        #E0 = phistart.codomain()
        #from montgomery import MontgomeryLine
        #MontgomeryLine.from_curve(E0).codomain().AC()
        AC=(7286130371200160433228726316916068871751*i + 624078470490561547110335006058160902979579, 1)
        A24 = ((AC[0]/AC[1]+2)/4)
        E0 = EllipticCurve(F, [0,AC[0]/AC[1],0,1,0])

        n=B
        print(f"- Test odd degree pairings, n={n}\n")
        test_odd_degree_pairings(E0, n, A*B//n, A24, k=2)

        print(f"- Test self pairings, n={n}\n")
        test_self_pairings(E0, n, A*B//n, A24, k=2)

        n = ZZ(2**(ea-10)*3**(eb-5))
        print(f"- Test even degree pairings, n={n}\n")
        test_even_degree_pairings(E0, n, A*B//n, A24, k=2)

        n=A
        print(f"- Test 2^m degree pairings, n={n}\n")
        test_twotothem_degree_pairings(E0, n, A*B//n, A24, k=2)



    def test2():
        ea=604
        eb=363
        A = ZZ(2**ea)
        B = ZZ(3**eb)
        p = A * B - 1
        F = GF(p**2, name="i", modulus=[1, 0, 1])
        i = F.gen()

        ## Generate a 'random' supersingular Montgomery curve
        #Estart = EllipticCurve(F, [1, 0])
        #T = 2**(ea-8)*B*Estart.random_point()
        #phistart = Estart.isogeny(T)
        #E0 = phistart.codomain()

        # Here are the constants of the isogeneous curve I obtained:
        AC=(4775824507378394905024196302775169017113690612014243371979334143754135607745378295403135755173105016360776545575708422936219469946689033116912090488612738615357486542055780497302355918897338123749541795523675656301935485717673057122988769196254169199195859719593550252770719250250220428757136531328260364786205424028327994746015214491863279902006320963659*i + 2870339932074782807384324686469914282837554201634749725356538597098997891754734061597220538855930893281199275333192018047037338283488668486978356519907480361613607778856419942105201865989903899904652167019456313840863188022088510236813626088811004104943350568201146834147966773761551034013844335896985720677134917366006711526240694167047845752643170176740, 1)
        A24 = ((AC[0]/AC[1]+2)/4)
        E0 = EllipticCurve(F, [0,AC[0]/AC[1],0,1,0])

        n=B
        print(f"- Test odd degree pairings, n={n}\n")
        test_odd_degree_pairings(E0, n, A*B//n, A24, k=2)

        print(f"- Test self pairings, n={n}\n")
        test_self_pairings(E0, n, A*B//n, A24, k=2)

        n = ZZ(2**(ea-100)*3**(eb-100))
        print(f"- Test even degree pairings, n={n}\n")
        test_even_degree_pairings(E0, n, A*B//n, A24, k=2)

        n=A
        print(f"- Test 2^m degree pairings, n={n}\n")
        test_twotothem_degree_pairings(E0, n, A*B//n, A24, k=2)

    def test3():
        p=16030569034403128277756688287498649515636838101184337499778392980116222246913;
        F=GF(p)
        E=EllipticCurve(F, [0,5])
        # E.j_invariant()=0, p=1 mod 4 so p splits in Q(i) and E ordinary
        r=E.cardinality() # this is prime: r.is_prime(): True
        #k=emb_degree(r,p) # 12
        k=12
        F2=GF(p**12, 't')
        E2=E.base_extend(F2)
        r2=E2.cardinality()

        AC=(F2(6123150921968337422568796176365652246109592885288416056556751737102623758294), F2(1))
        A24 = ((AC[0]/AC[1]+2)/4)
        E2M=EllipticCurve(F2, [0,AC[0],0,1,0]) #this is a twist of E over Fp

        print(f"- Test pairings, n={r}\n")
        test_odd_degree_pairings(E2M, r, r2//r**2, A24, k=k)
        print(f"- Test self pairings, n={r}\n")
        test_self_pairings(E2M, r, r2//r**2, A24, k=k)

        P=E.random_point()
        Q=(r2//r**2)*E2.random_point() # multiply by cofactor to get point of r-torsion
        #E2(P).weil_pairing(Q,r)
        #E2(P).tate_pairing(Q,r,k)
        phi=E2.isomorphism_to(E2M)
        print(f"- Test pairings on G1xG3, n={r}\n")
        test_odd_degree_pairings(E2, r, r2//r**2, A24, k=k, points=[phi(E2(P)),phi(Q)])

    def test4():
        #BLS12-377 (communicated by: Jianming Lin)
        p = 0x01ae3a4617c510eac63b05c06ca1493b1a22d9f300f5138f1ef3622fba094800170b5d44300000008508c00000000001
        K = GF(p)
        a = K(0x00); b = K(0x01)
        E = EllipticCurve(K, (a, b))
        #from montgomery import MontgomeryLine
        #AC=MontgomeryLine.from_curve(E).codomain().AC()
        #print(AC)
        AC=(K(228097355113300204138531148905234651262148041026195375645000724271212049151994375092458297304264351187709081232384), K(1))
        A24 = ((AC[0]/AC[1]+2)/4)
        print(A24)
        EM = EllipticCurve(K,[0,AC[0],0,1,0])
        n = E.cardinality()
        e1 = 3195374304363544576; e1=n//n*e1
        PM1 = EM(K(5930997225675844053789879947602580912291714081761957693283726497102128480619475637721419062499593583917707964165), K(121811180146149741170160932132502736367485621499581559068792696377395732026578317331647179196347902443805681940168))
        #print(e1*PM1)
        print(f"- Test pairings, n={e1}\n")
        P=EM.random_point()
        test_even_degree_pairings(EM, e1, n//e1**2, A24,k=1,P=P,Q=PM1,correct=True)


    print("########### Supersingular pairings tests on a small field ############")
    test1()
    print("########### Supersingular pairings tests on a large field ############")
    test2()
    print("########### Testing a BN curve ############")
    test3()
    print("########### Testing embedding degree 1 curve ############")
    test4()
