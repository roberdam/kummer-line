# from (P, σ(P)), double the point and compute σ(2P)
def double(Ps):
    P, s = Ps
    return (P+P, s**4 * 2 *P[1])
#NB: if we compute the square of the Tate pairing, we only need σ^2 and so y_P^2 which we can express in term of x_P

def diff_add(Ps, Qs, PmQs):
    P, sP = Ps
    Q, sQ = Qs
    PmQ, sPmQ = PmQs
    s=sP**2 * sQ**2 * -(P[0]-Q[0]) / sPmQ
    return (P+Q, s)

# Essentially, in the cubical Kummer ladder, we would like to start with
# P~=(x(P),σ(P)), Q~=(x(Q),σ(Q)), P-Q~=(x(P-Q),σ(P-Q))
# and compute P+Q~=(x(P+Q),σ(P+Q))

# returns nQ, P+nQ + their σ value
# Notice that we only need x(P+nQ), not y(P+nQ), to compute σ(P+nQ)
def ladder3(n, P, Q, PQ):
    if n == 0:
        return P.curve()(0), P
    if n == 1:
        return Q, PQ
    if n < 0:
        PmQ = diff_add(P, Q, PQ)
        return ladder3(abs(n), P, Q, PmQ)

    nQ=Q
    nQQ=double(Q)
    PnQQ=diff_add(PQ, Q, P)

    #if n==2:
    #    return (nQQ, PnQQ)

    # Montgomery-ladder
    for bit in bin(n-1)[3:]:
        R = diff_add(nQQ, nQ, Q)
        if bit == "0":
            PnQQ=diff_add(PnQQ, nQ, PQ)
            nQ=double(nQ)
            nQQ=R
        else:
            PnQQ=diff_add(PnQQ, nQQ, P)
            nQQ=double(nQQ)
            nQ=R
    return (nQQ, PnQQ)

# returns (n-1)Q, nQ
# warning: this is not like the other ladder, but more like ladder_bis
def ladder(n, Q):
    n = abs(n)
    if n == 0:
        return (Q, Q.curve()(0))
    if n == 1:
        return (Q.curve()(0), Q)

    nQ=Q
    nQQ=double(Q)

    # if n==2:
    #     return (nQ, nQQ)
    for bit in bin(n-1)[3:]:
        R = diff_add(nQQ, nQ, Q)
        if bit == "0":
            nQ=double(nQ)
            nQQ=R
        else:
            nQQ=double(nQQ)
            nQ=R
    return (nQ, nQQ)

# general method:
# since σ(0)=0, we need to translate by a random point
# so that we compute σ(P+T+nQ)/σ(T+nQ) rather than σ(P+nQ)/σ(nQ)
# or rather σ(P+T+nQ)σ(T)/σ(T+nQ)σ(P+T)
# but we normalize by σ(T)=σ(P+T)=σ(P+Q)=1
# this works
def non_reduced_tate_T(n, P, Q, T):
    nQ, PTnQ = ladder3(n, (P+T,1), (Q,1), (P+T+Q,1))
    nQ, TnQ = ladder3(n, (T,1), (Q,1), (T+Q,1))
    return PTnQ[1]/TnQ[1]

# other idea: plug T=-Q in the above equation, so we compute
# σ(P+(n-1)Q)σ(-Q)/σ((n-1)Q)σ(P-Q)
# if we normalize by σ(P+Q)=σ(Q)=1, then
# σ(-Q)=-σ(Q) car σ est odd, σ(P-Q)=x(Q)-x(P)
# This works too!
def non_reduced_tate(n, P, Q):
    nQ, PnQ = ladder3(n-1, (P,1), (Q,1), (P+Q,1))
    return -PnQ[1]/(nQ[1]*(Q[0]-P[0]))

def tate(n, P, Q,  k=1):
    xP=P[0]
    p=xP.base_ring().characteristic()
    d=(p**k-1)/n
    e = non_reduced_tate(n, P, Q)
    return e**d

if __name__ == "__main__" and "__file__" in globals():
    import time
    from sage.all import ZZ, GF, EllipticCurve, proof

    ea=74
    eb=41
    A = ZZ(2**ea)
    B = ZZ(3**eb)
    p = A * B - 1
    F = GF(p**2, name="i", modulus=[1, 0, 1])
    i = F.gen()

    AC=(7286130371200160433228726316916068871751*i + 624078470490561547110335006058160902979579, 1)
    A24 = ((AC[0]/AC[1]+2)/4)
    E0 = EllipticCurve(F, [0,AC[0]/AC[1],0,1,0])
    n=B
    cofactor = A*B//n

    P = E0.random_point()
    Q = cofactor * E0.random_point()
    #T = E0.random_point()

    print("Testing the Tate pairing via the σ-cubical arithmetic")
    k=2
    t1=Q.tate_pairing(P, n, k=k)
    t2=tate(n,P,Q, k=k)
    assert t1 == t2
