from kummer import StandardKummerLine
from montgomery import MontgomeryCurve, MontgomeryLine, MontgomeryPoint
from montgomery2 import Montgomery2Line, Montgomery2Point
from theta import ThetaLine, ThetaPoint, TwistedHThetaLine
from sage.all import EllipticCurve, ZZ, GF, proof

def zeta(P):
    E=P.curve()
    x=P[0]; y=P[1]
    return E(-x, i*y)

def test_up_to_automorphisms(P1, P2):
    E = P1.curve()
    j = E.j_invariant()
    if j == 1728:
        return P1 == P2 or P1 == -P2 or P1==zeta(P2) or P1==-zeta(P2)
    elif j == 0:
        print("TODO!")
    else:
        return P1 == P2 or P1 == -P2

def test_isogeny(P, convert, T, isogeny, target=None):
    K=convert.codomain()
    Te=T.lift_to_elliptic_point(target=target)
    E=Te.curve()
    f=E.isogeny(Te)
    jnew = f.codomain().j_invariant()
    assert isogeny.codomain().j_invariant() == jnew
    print(f"Same isogeneous j-invariant: {jnew}")

    print(f"Testing images")
    Pm=convert(P)
    if E == P.curve():
        Pme=P
    else:
        Pme=Pm.lift_to_elliptic_point(target=target)
    imP=f(Pme)
    ET1=imP.curve()
    imPm = isogeny(Pm)
    imPme = imPm.lift_to_elliptic_point(target=ET1)
    assert test_up_to_automorphisms(imPme, imP)

def test_isogenies(E0, P):
    print("=========== Isogenies in the Montgomery model ============")
    convert = Montgomery2Line.from_curve(E0)
    KM=convert.codomain()

    print("- Testing isogeny TT1")
    T1 = KM.T1()
    T = 2**(ea-3)*B*E0.random_point()
    while(convert(2*T) != KM.TT1()):
        T = 2**(ea-3)*B*E0.random_point()
    fM = KM.isogeny_T1(convert(T))
    test_isogeny(P, convert, T1, fM)
    test_isogeny(P, convert, T1, fM, target=E0)


    print("=========== Isogenies in the Montgomery2 model ============")
    convert = Montgomery2Line.from_curve(E0)
    KM=convert.codomain()

    print("- Testing isogeny T2")
    T2 = KM.T2()
    fM = KM.isogeny_T2() #Montgomery2 -> Montgomery
    test_isogeny(P, convert, T2, fM, target=E0)
    print("- Testing radical isogeny T2")
    radfM = KM.radical_isogeny_T2() #Montgomery2 -> Montgomery2
    test_isogeny(P, convert, T2, radfM, target=E0)

    print("- Testing isogeny TT2")
    T = 2**(ea-2)*B*E0.random_point()
    while(convert(2*T) != T2):
        T = 2**(ea-2)*B*E0.random_point()
    fM = KM.isogeny_T2(convert(T))
    fMt = KM.translated_isogeny_T2(convert(T))
    Pm=convert(P)
    imPm = fM(Pm)
    imPm = fMt(Pm).translate()
    assert imPm == fM(Pm)
    test_isogeny(P, convert, T2, fM, target=E0)

    print("- Testing isogeny T1")
    T1 = KM.T1()
    fM = KM.isogeny_T1()
    fMt = KM.translated_isogeny_T1()
    Pm=convert(P)
    imPm = fM(Pm)
    imPm = fMt(Pm).translate()
    assert imPm == fM(Pm)
    test_isogeny(P, convert, T1, fM, target=E0)

    print("=========== Isogenies in the theta model ============")
    convert = ThetaLine.from_curve(E0)
    KT=convert.codomain()

    print("- Testing (radical) isogeny T1")
    T1 = KT.T1()
    fT = KT.isogeny_T1()
    test_isogeny(P, convert, T1, fT, target=E0)

    print("- Testing (radical) isogeny T2")
    T2 = KT.T2()
    fT = KT.isogeny_T2()
    test_isogeny(P, convert, T2, fT, target=E0)

    print("- Testing isogeny TT1")
    T1 = KT.T1()
    T = 2**(ea-3)*B*E0.random_point()
    while(convert(2*T) != KT.TT1()):
        T = 2**(ea-3)*B*E0.random_point()
    fT = KT.isogeny_T1(Tb=convert(T))
    test_isogeny(P, convert, T1, fT, target=E0)

    print("- Testing isogeny TT2")
    T2 = KT.T2()
    T = 2**(ea-3)*B*E0.random_point()
    while(convert(2*T) != KT.TT2()):
        T = 2**(ea-3)*B*E0.random_point()
    fT = KT.isogeny_T2(Tb=convert(T))
    test_isogeny(P, convert, T2, fT, target=E0)

    print("=========== Isogenies in the twisted' theta model ============")
    convert = TwistedHThetaLine.from_curve(E0)
    KT=convert.codomain()

    print("- Testing isogeny T1")
    T1 = KT.T1()
    fT = KT.isogeny_T1()
    test_isogeny(P, convert, T1, fT, target=E0)

    print("- Testing isogeny TT2")
    T2 = KT.T2()
    T = 2**(ea-2)*B*E0.random_point()
    while(convert(2*T) != T2):
        T = 2**(ea-3)*B*E0.random_point()
    fT = KT.isogeny_T2(Tb=convert(T))
    test_isogeny(P, convert, T2, fT, target=E0)

    print("- Testing radical isogeny T2")
    fT = KT.radical_isogeny_T2()
    test_isogeny(P, convert, T2, fT, target=E0)

#def test_isogenies(E0, P):
#    convert = TwistedHThetaLine.from_curve(E0)
#    KT=convert.codomain()
#    T2 = KT.T2()
#    print("- Testing radical isogeny T2")
#    fT = KT.radical_isogeny_T2()
#    test_isogeny(P, convert, T2, fT, target=E0)

proof.all(False)

ea=604
eb=363

A = ZZ(2**ea)
B = ZZ(3**eb)
p = A * B - 1
F = GF(p**2, name="i", modulus=[1, 0, 1])
i = F.gen()

Estart = EllipticCurve(F, [1, 0])
E0 = Estart

print(f"============= Isogenies on {E0} =============")
P=E0.random_point()
test_isogenies(E0, P)

print("=========== Testing a new curve via a random isogeny =========")
# T = 2**(ea-100)*B*Estart.random_point()
# phistart = Estart.isogeny(T, algorithm="factored")
T = 2**(ea-8)*B*Estart.random_point()
phistart = Estart.isogeny(T)

E0 = phistart.codomain()
print(f"=> Tests on {E0}, j={E0.j_invariant()}")
P=E0.random_point()
test_isogenies(E0, P)
