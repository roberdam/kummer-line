from montgomery import AffineMontgomeryLine, MontgomeryLine
from montgomery2 import Montgomery2Line
from theta import AffineThetaLine, ThetaLine, AffineTwistedHThetaLine
from biextension import Biextension
from sage.all import EllipticCurve, ZZ, GF, proof

# Q is the n-torsion point
def compute_tate_pairings(K, n, P, Q, k=1, d=None, exp_function=None, zero=None, scale = False):
    E=P.curve()
    #K=ThetaLine.from_curve(E)
    P0=K(P)
    Q0=K(Q)
    PQ0=K(P+Q)

    # note: for even pairing, we need to start with an element g_P,Q which is a
    # tensorial square of h_P,Q in level 1; hence if we scale we should
    # scale by an element which is globally a square over the base ring
    if scale:
        # random scalings to test the algorithm
        R=P0.parent().base_ring()
        lambdaP=R.random_element()
        lambdaQ=R.random_element()
        lambdaPQ=R.random_element()
        P0=P0.scale(lambdaP)
        Q0=Q0.scale(lambdaQ)
        PQ0=PQ0.scale(lambdaPQ)

    g=Biextension(P0, Q0, PQ0, zero=zero)

    t1=Q.tate_pairing(P, n, k=k)
    t2=g.tate_pairing(n, k=k, d=d, exp_function=exp_function)
    # the reduced tate pairing is also given by using the exponentiation by p^k-1 in the biextension
    t2bis = g.non_reduced_tate_pairing(p**k-1, exp_function=exp_function)
    return t1, t2, t2bis

def compute_weil_pairings(K, n, P, Q, k=1, exp_function=None, zero=None, scale=False):
    E=P.curve()
    #K=ThetaLine.from_curve(E)
    P0=K(P)
    Q0=K(Q)
    PQ0=K(P+Q)

    if scale:
        # random scalings to test the algorithm
        R=P0.parent().base_ring()
        lambdaP=R.random_element()
        lambdaQ=R.random_element()
        lambdaPQ=R.random_element()
        P0=P0.scale(lambdaP)
        Q0=Q0.scale(lambdaQ)
        PQ0=PQ0.scale(lambdaPQ)

    t1=Q.weil_pairing(P, n)
    t2=Biextension(P0, Q0, PQ0, zero=zero).weil_pairing(n, exp_function=exp_function)
    return t1, t2

proof.all(False)

ea=604
eb=363

A = ZZ(2**ea)
B = ZZ(3**eb)
p = A * B - 1
F = GF(p**2, name="i", modulus=[1, 0, 1])

def test_pairings(E0):
    P = E0.random_point()
    Q = E0.random_point()
    R = E0.random_point()
    Q = A*Q
    R = B*R
    #while ((A//2)*R == E0(0)):
    #    R = E0.random_point()
    #    R = B*R

    KT=AffineThetaLine.from_curve(E0)
    KM=AffineMontgomeryLine.from_curve(E0)
    #K2=Montgomery2Line.from_curve(E0)
    K2=AffineTwistedHThetaLine.from_curve(E0)
    def exp_function(g, n):
        return g.ladder(n)
    def exp_function_bis(g, n):
        g1, g2=g.full_ladder_bis(n)
        return g2

    def fast_ladder(g, n):
        return g.fast_ladder(n)

    # print("- Test even pairings in theta via ladder3")
    # t1, t2, _t2bis = compute_tate_pairings(KT, A, P, R, k=2)
    # assert t1 == t2
    # # assert t2 == _t2bis #This does not work in even degree because of the translation trick
    # t1, t2 = compute_weil_pairings(KT, A, B*P, R)
    # assert t1 == t2

    print("- Test pairings in theta via ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(KT, B, P, Q, k=2, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t3, t4, _ = compute_tate_pairings(KT, B, P, Q+Q, k=2, scale=True)
    assert t4 == t2*t2
    t5, t6, _ = compute_tate_pairings(KT, B, P+P, Q, k=2, scale=True)
    assert t6 == t2*t2
    # Weil pairing in theta
    t1, t2 = compute_weil_pairings(KT, B, A*P, Q, scale=True)
    assert(t1**2 == t2)

    print("- Test pairings in theta via ladder3")
    t1, t2, t2bis = compute_tate_pairings(KT, B, P, Q, k=2, exp_function = fast_ladder, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2

    print("- Test pairings in theta via the biextension ladder")
    t1, t2, t2bis = compute_tate_pairings(KT, B, P, Q, k=2, exp_function=exp_function, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t1, t2 = compute_weil_pairings(KT, B, A*P, Q, exp_function=exp_function)
    assert t1**2 == t2

    print("- Test pairings in theta via the biextension ladder bis")
    t1, t2, t2bis = compute_tate_pairings(KT, B, P, Q, k=2, exp_function = exp_function_bis, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2

    print("- Test even pairings in theta via ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(KT, A, P, R, k=2)
    assert t1 == t2
    t1, t2 = compute_weil_pairings(KT, A, B*P, R)
    assert t1 == t2

    print("- Test pairings in Montgomery via ladder3/ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(KM, B, P, Q, k=2, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t1, t2 = compute_weil_pairings(KM, B, A*P, Q, scale=True)
    assert t1**2 == t2
    t1, t2, t2bis = compute_tate_pairings(KM, B, P, Q, k=2, exp_function = fast_ladder, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2

    print("- Test pairings in Montgomery via the biextension ladder/ladder bis")
    t1, t2, t2bis = compute_tate_pairings(KM, B, P, Q, k=2, exp_function=exp_function, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t1, t2 = compute_weil_pairings(KM, B, A*P, Q, exp_function=exp_function, scale=True)
    assert t1**2 == t2
    t1, t2, t2bis = compute_tate_pairings(KM, B, P, Q, k=2, exp_function = exp_function_bis, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2

    print("- Test even pairings in Montgomery via ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(KM, A, P, R, k=2)
    assert t1 == t2
    t1, t2 = compute_weil_pairings(KM, A, B*P, R)
    assert t1 == t2

    print("- Test pairings in TwistedHTheta via ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(K2, B, P, Q, k=2, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t1, t2 = compute_weil_pairings(K2, B, A*P, Q, scale=True)
    assert(t1**2 == t2)

    print("- Test pairings in TwistedHTheta via the biextension ladder")
    t1, t2, t2bis = compute_tate_pairings(K2, B, P, Q, k=2, exp_function=exp_function, scale=True)
    assert t2 == t2bis
    assert t1**2 == t2
    t1, t2 = compute_weil_pairings(K2, B, A*P, Q, exp_function=exp_function, scale=True)
    assert t1**2 == t2

    print("- Test even pairings in Twisted'Theta via ladder3_bis")
    t1, t2, t2bis = compute_tate_pairings(K2, A, P, R, k=2)
    assert t1 == t2
    t1, t2 = compute_weil_pairings(KT, A, B*P, R)
    assert t1 == t2

Estart = EllipticCurve(F, [1, 0])
E0 = Estart
print(f"============= Pairings on {E0} =============")
test_pairings(E0)

print("=========== Testing a new curve via a random isogeny =========")
# T = 2**(ea-100)*B*Estart.random_point()
# phistart = Estart.isogeny(T, algorithm="factored")
T = 2**(ea-8)*B*Estart.random_point()
phistart = Estart.isogeny(T)

E0 = phistart.codomain()
print(f"=> Tests on {E0}, j={E0.j_invariant()}")
test_pairings(E0)
