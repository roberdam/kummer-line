from sage.all import EllipticCurve, ZZ, GF, proof
from theta import ThetaLine, TwistedHThetaLine
from montgomery2 import Montgomery2Line
scalar_tests=[0, 1, 2, 7, 5, -5, -13, 10, 16, 27, 33, 91]

# test the time/memory tradeoff ladders
def test_ladder(P, convert):
    K = convert.codomain()
    Pt = convert(P)
    Pe = Pt.lift_to_elliptic_point()

    for scalar in scalar_tests:
        print(f"Testing precomp_ladder for l={scalar}")
        lPt = Pt.precomp_ladder(scalar)
        lPet = convert(scalar * P)
        assert lPt == lPet

def test_ladders(E0, P):
    print("============= Theta line model =============")
    test_ladder(P, ThetaLine.from_curve(E0))
    print("============= Thetatw' line model =============")
    test_ladder(P, TwistedHThetaLine.from_curve(E0))
    print("============= Montgomery2 line model =============")
    test_ladder(P, Montgomery2Line.from_curve(E0))

proof.all(False)

ea=604
eb=363

A = ZZ(2**ea)
B = ZZ(3**eb)
p = A * B - 1
F = GF(p**2, name="i", modulus=[1, 0, 1])
i = F.gen()
Estart = EllipticCurve(F, [1, 0])
E0 = Estart

print(f"============= Arithmetic on {E0} =============")
P = E0.random_point()
test_ladders(E0, P)

print("=========== Testing a new curve via a random isogeny =========")
T = 2**(ea-8)*B*Estart.random_point()
phistart = Estart.isogeny(T)

E0 = phistart.codomain()
print(f"=> Tests on {E0}, j={E0.j_invariant()}")
P = E0.random_point()
test_ladders(E0, P)
