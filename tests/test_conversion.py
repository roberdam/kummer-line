from kummer import StandardKummerLine
from montgomery import MontgomeryCurve, MontgomeryLine, MontgomeryPoint
from montgomery2 import Montgomery2Line, Montgomery2Point
from theta import ThetaLine, ThetaPoint, TwistedHThetaLine
from sage.all import EllipticCurve, ZZ, GF, proof

proof.all(False)

ea=604
eb=363

A = ZZ(2**ea)
B = ZZ(3**eb)
p = A * B - 1
F = GF(p**2, name="i", modulus=[1, 0, 1])
i = F.gen()

Estart = EllipticCurve(F, [1, 0])
E0 = Estart
P = E0.random_point()
Q = E0.random_point()

def test_montgomery_curve(E0):
    assert MontgomeryCurve.is_montgomery(E0)
    KM = MontgomeryLine.from_montgomery_curve(E0)
    Pm = KM(P)
    Qm = KM(Q)
    Pe = Pm.lift_to_elliptic_point()
    assert Pe == P or Pe == -P

    print(f"Testing lifting points")
    Pme = Pm.lift_to_elliptic_point()
    assert Pm == KM(Pme)

    print("- Testing conversion from Montgomery to theta")
    MtT = ThetaLine.from_montgomery(E0)
    KT = MtT.codomain()
    Pt = MtT(P)
    Qt = MtT(Q)

    to_theta = KM.to_theta(th=KT)
    to_montgomery = KT.to_montgomery()

    Pmt = to_theta(Pm)
    #print(Pt)
    #print(Pmt)
    assert Pmt == Pt
    to_theta_inv=to_theta.dual()
    assert Pm == to_theta_inv(Pt)
    assert Pm == to_theta.inverse_image(Pt)

    print("- Testing conversion from theta to Montgomery")
    Ptm = to_montgomery(Pt)
    assert Ptm == Pm
    #print(Pm.ratio(Ptm)) #some factor
    Qtm = to_montgomery(Qt)
    assert Qtm == Qm
    #print(Qm.ratio(Qtm)) #same factor

    print("- Testing conversion from Montgomery line to short Weierstrass curve")
    to_short = KM.lift_to_short_weierstrass()
    Pshort = to_short(Pm)
    E2 = to_short.codomain()
    phi = E0.isomorphism_to(E2)
    assert Pshort == phi(P) or Pshort == -phi(P)

def test_conversions(E0, P):
    print("- Testing different variant for conversion between short Weierstrass curve to Montgomery curve")
    K0=StandardKummerLine(E0)

    print("-> Via just the curve")
    fM = MontgomeryCurve.from_curve(E0)
    EM = fM.codomain()
    PM = fM(P)
    assert EM.j_invariant() == E0.j_invariant()
    fshort = EM.to_short_weierstrass()
    E02 = fshort.codomain()
    assert E02.j_invariant() == E0.j_invariant()
    assert E0 == E02
    Pshort=fshort(PM)
    assert P == Pshort
    Pshort2 = PM.to_short_weierstrass(E0)

    print("-> Via four torsion")
    T = E0.random_point()
    T = (A//4*B)*T
    while (2*T).is_zero():
        T = E0.random_point()
        T = (A//4*B)*T

    fM2 = MontgomeryCurve.from_four_torsion(T)
    fshort2 = fM2.codomain().to_short_weierstrass()
    PM2=fM2(P)
    assert fM2.codomain().j_invariant() == E0.j_invariant()
    assert P == fshort2(PM2)

    print("-> Via two torsion")
    fM3 = MontgomeryCurve.from_two_torsion(2*T)
    PM3=fM3(P)
    fshort3 = fM3.codomain().to_short_weierstrass()
    assert fM3.codomain().j_invariant() == E0.j_invariant()
    assert P == fshort3(PM3)

    print("- Testing conversion from Montgomery line to short Weierstrass line")
    KM = EM.montgomery_line()
    PKM = KM(PM)
    fKshort = KM.to_short_weierstrass_line()
    PKshort = fKshort(PKM)
    assert E02.j_invariant() == E0.j_invariant()
    kummer = StandardKummerLine(E02)
    assert PKshort == fKshort.codomain()(Pshort)
    assert PKshort.proj_equal(kummer(Pshort))
    assert PKshort == kummer(Pshort)
    to_K0 = KM.to_kummer_line(K0)
    assert to_K0(PKM) == K0(P)

    print("- Testing conversion from Montgomery line to Montgomery2 line")
    KM2 = KM.montgomery2()
    PKM2 = KM2(PKM)
    assert KM2.a() == KM.a()
    assert KM2.j_invariant() == KM.j_invariant()
    assert PKM.proj_equal(PKM2)

    print("- Testing different variants for conversion between short Weierstrass curve to Montgomery line")
    print("-> Via four torsion")
    fM4 = MontgomeryLine.from_four_torsion(T)
    fshort4 = fM4.codomain().to_short_weierstrass_line()
    PM4=fM4(P)
    assert fM4.codomain().j_invariant() == E0.j_invariant()
    KM4=fshort4.codomain()
    EM4=KM4.curve()
    phi=E0.isomorphism_to(EM4)
    #print(fshort4(PM4).proj_equal(KM4(phi(P))))
    phi2=K0.isomorphism_to(KM4)
    assert fshort4(PM4) == KM4(phi(P))
    assert fshort4(PM4) == phi2(P)
    assert(fM4(T).proj_equal((1,1)))
    assert(fM4(2*T).proj_equal((0,1)))

    print("-> Via two torsion")
    fM5 = MontgomeryLine.from_two_torsion(2*T)
    fshort5 = fM5.codomain().to_short_weierstrass_line()
    PM5=fM5(P)
    assert fM5.codomain().j_invariant() == E0.j_invariant()
    KM5=fshort5.codomain()
    phi2=K0.isomorphism_to(KM5)
    assert fshort5(PM5) == phi2(P)
    assert(fM5(2*T).proj_equal((0,1)))

    print("-> Via just the Kummer line")
    fM6 = MontgomeryLine.from_curve(K0)
    fshort6 = fM6.codomain().to_short_weierstrass_line()
    PM6=fM6(P)
    assert fM6.codomain().j_invariant() == E0.j_invariant()
    KM6=fshort6.codomain()
    phi2=K0.isomorphism_to(KM6)
    assert fshort6(PM6) == phi2(P)

    print("-> Via just the curve")
    fM7 = MontgomeryLine.from_curve(E0)
    fshort7 = fM7.codomain().to_short_weierstrass_line()
    PM7=fM7(P)
    assert fM7.codomain().j_invariant() == E0.j_invariant()
    KM7=fshort7.codomain()
    phi2=K0.isomorphism_to(KM7)
    assert fshort7(PM7) == phi2(P)

    print("Testing conversion from short Weierstrass to Montgomery2Line")
    f2M = Montgomery2Line.from_curve(E0)
    f2short = f2M.codomain().to_short_weierstrass_line()
    P2M=f2M(P)
    assert f2M.codomain().j_invariant() == E0.j_invariant()
    K2Mshort=f2short.codomain()
    phi2=K0.isomorphism_to(K2Mshort)
    assert f2short(P2M) == phi2(P)

    print("Testing conversions between short Weierstrass, Montgomery2Line and the twisted' theta model")
    P2t = P2M.to_twisted_tr()
    assert P2t.proj_equal(P2M)
    P2tM = P2t.to_montgomery2_tr()
    assert P2M == P2tM

    P2t = P2M.to_twisted()
    P2Mtr = P2M.translate()
    assert P2t.proj_equal(P2Mtr)
    P2tM = P2t.to_montgomery2()
    assert P2t.translate().proj_equal(P2tM)
    assert P2M == P2tM

    f2t = TwistedHThetaLine.from_curve(E0)
    f2short = f2t.codomain().lift_to_elliptic()
    P2t=f2t(P)
    assert f2t.codomain().j_invariant() == E0.j_invariant()
    E2tshort=f2short.codomain()
    phi2=E0.isomorphism_to(E2tshort)
    assert f2short(P2t) == phi2(P) or f2short(P2t) == -phi2(P)

    f2short = f2t.codomain().lift_to_elliptic(target=E0)
    # print(f2short(P2t))
    # print(f2short(P2t).curve())
    # print(P)
    # print(P.curve())
    assert f2short(P2t) == P or f2short(P2t) == -P

    f2short = f2t.codomain().to_kummer_line(target=K0)
    assert f2short(P2t) == K0(P)

    print("- Testing different variant for conversion from short Weierstrass curve to Theta line")
    print("-> Via just the curve")
    fT = ThetaLine.from_curve(E0)
    KT = fT.codomain()
    fshort = KT.to_kummer_line()
    PT = fT(P)
    KE = fshort.codomain()
    assert KT.j_invariant() == E0.j_invariant()
    assert KE.j_invariant() == E0.j_invariant()
    phi2=K0.isomorphism_to(KE)
    assert fshort(PT) == phi2(P)

    print("   Sanity checks for conversion from theta to Montgomery")
    to_montgomery=KT.to_montgomery()
    #print(to_montgomery(KT.T1()).coords())
    #print(to_montgomery(KT.TT1()).coords())
    #print(to_montgomery(KT.TT1bis()).coords())
    #print(to_montgomery(KT.T2()).coords())
    #print(to_montgomery(KT.TT2()).coords())
    #print(to_montgomery(KT.TT2bis()).coords())
    #print(to_montgomery(KT.T1()).x())
    #print(to_montgomery(KT.TT1()).x())
    #print(to_montgomery(KT.TT1bis()).x())
    #print(to_montgomery(KT.T2()).x())
    #print(to_montgomery(KT.TT2()).x())
    #print(to_montgomery(KT.TT2bis()).x())
    assert to_montgomery(KT.T1()).x() == 0
    assert to_montgomery(KT.TT1()).x() == F(-1)
    assert to_montgomery(KT.TT1bis()).x() == F(1)

    to_montgomery2=KT.to_montgomery2()
    assert to_montgomery(PT).proj_equal(to_montgomery2(PT))
    assert to_montgomery.codomain().a()==to_montgomery2.codomain().a()

    print(f"    Testing lifting points")
    PTe = PT.lift_to_elliptic_point()
    reduce = KT.reduce_from_elliptic()
    assert PT == reduce(PTe)
    PTe = PT.lift_to_elliptic_point(target=E0)
    reduce = KT.reduce_from_elliptic(target=E0)
    #assert PT == reduce(PTe) #TODO

    print("-> Via the four torsion")
    T2 = E0.random_point()
    T2 = (A//4*B)*T2
    while T.weil_pairing(T2, ZZ(4)) != i:
        T2 = E0.random_point()
        T2 = (A//4*B)*T2

    fT2 = ThetaLine.from_four_torsion(T, T2)
    assert fT2.codomain().j_invariant() == E0.j_invariant()
    assert fT2(T).proj_equal((1,0))
    assert fT2(T2).proj_equal((1,1))
    KT2=fT2.codomain()
    fshort2 = KT2.to_kummer_line()
    PT2 = fT2(P)
    KE2 = fshort2.codomain()
    phi2=K0.isomorphism_to(KE2)
    assert fshort2(PT2) == phi2(P)


print(f"============= Conversions on {E0} =============")
test_montgomery_curve(E0)

print("=========== Testing a new curve via a random isogeny =========")
# T = 2**(ea-100)*B*Estart.random_point()
# phistart = Estart.isogeny(T, algorithm="factored")
T = 2**(ea-8)*B*Estart.random_point()
phistart = Estart.isogeny(T)
E0 = phistart.codomain()
print(f"=> Tests on {E0}, j={E0.j_invariant()}")
P = E0.random_point()
test_conversions(E0, P)

