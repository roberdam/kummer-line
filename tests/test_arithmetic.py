from morphism import TrivialChangeModel
from kummer import StandardKummerLine
from montgomery import MontgomeryCurve, MontgomeryLine, MontgomeryPoint
from montgomery2 import Montgomery2Line, Montgomery2Point
from theta import ThetaLine, ThetaPoint, TwistedHThetaLine
from sage.all import EllipticCurve, ZZ, GF, proof

scalar_tests=[0, 1, 2, 7, 5, -5, -13, 10, 16, 27, 33, 91]

def test_double_diff_add(K, P, Q):
    print(f"Testing double and diff_add")
    P0 = K(P)
    Q0 = K(Q)
    PmQ0 = K(P-Q)
    PP0 = P0.double()
    PQ0 = P0.diff_add(Q0, PmQ0)
    assert PP0 == K(P+P)
    assert PQ0 == K(P+Q)

def test_ladder(K, P, Q):
    P0 = K(P)
    Q0 = K(Q)
    PmQ0 = K(P-Q)
    PQ0=K(P+Q)
    for scalar in scalar_tests:
        print(f"Testing ladder3 for l={scalar}")
        lP0, llP0 = P0.full_ladder(scalar)
        assert lP0 == K(scalar*P)
        #the way we compute the full ladder we don't really compute nP, (n+1)P but |n|P, (|n|+1)P
        assert llP0 == K(abs(scalar)*P+P)
        llP0, lP0 = P0.full_ladder_bis(scalar)
        assert lP0 == K(scalar*P)
        assert llP0 == K(abs(scalar)*P-P)

        lP0 = P0 * scalar
        lPe0 = K(scalar * P)
        lP0b, lPQ0 = Q0.full_ladder3(scalar, P0, PmQ0)
        lP0b2, lPQ02 = Q0.full_ladder3_bis(scalar, P0, PQ0)
        lPQe0 = K(scalar * P + Q)
        assert lP0 == lPe0
        assert lP0 == lP0b
        assert lP0 == lP0b2
        assert lPQ0 == lPQe0
        assert lPQ02 == lPQe0

def test_lifting(K, P):
    print(f"Testing lifting points")
    Pe = P.lift_to_elliptic_point()
    reduce = K.codomain().reduce_from_elliptic()
    assert P == reduce(Pe)

def test_arithmetic(E0, P, Q):
    print("============= Kummer line model =============")
    Kshort = StandardKummerLine(E0)
    test_double_diff_add(Kshort, P, Q)
    Kshort = StandardKummerLine.from_curve(E0)
    test_lifting(Kshort, Kshort(P))
    #test_ladder(Kshort, P, Q) #very slow with the way it is implemented...

    print("====== Montgomery =========")
    convert = MontgomeryLine.from_curve(E0)
    KM=convert.codomain()
    Pm = convert(P)
    test_lifting(convert, Pm)

    print(f"Testing translation")
    T1=KM.T1()
    T1e = T1.lift_to_elliptic_point()
    assert Pm.translate_T1() == KM(Pm.lift_to_elliptic_point()+T1e)

    test_double_diff_add(convert, P, Q)
    test_ladder(convert, P, Q)

    Am, Cm = KM.AC()
    KMbis = MontgomeryLine((5*Am, 5*Cm))
    print(f"=> Testing with (A,C)={KMbis.AC()}")
    convertbis=convert.compose(TrivialChangeModel.identity(KMbis))
    test_double_diff_add(convertbis, P, Q)
    test_ladder(convertbis, P, Q)

    print("============= Theta model =============")
    MtT = ThetaLine.from_curve(E0)
    KT=MtT.codomain()
    Pt = MtT(P)
    test_lifting(MtT, Pt)

    print(f"Testing translation")
    Pe = Pt.lift_to_elliptic_point()
    reduce = KT.reduce_from_elliptic()
    T1=KT.T1()
    T1e = T1.lift_to_elliptic_point()
    assert Pt.translate_T1() == reduce(Pe+T1e)
    T2=KT.T2()
    T2e = T2.lift_to_elliptic_point()
    assert Pt.translate_T2() == reduce(Pe+T2e)
    T3=KT.T3()
    T3e = T3.lift_to_elliptic_point()
    assert Pt.translate_T3() == reduce(Pe+T3e)

    test_double_diff_add(MtT, P, Q)
    test_ladder(MtT, P, Q)

    print("============= Montgomery cap Legendre model =============")
    convert=Montgomery2Line.from_curve(E0)
    KM2=convert.codomain()
    PM2 = convert(P)
    test_lifting(convert, PM2)
    Pm2 = convert(P)
    Qm2 = convert(Q)
    PmQm2 = convert(P-Q)

    print(f"Testing translation")
    T = KM2.elliptic_two_torsion()
    assert T.curve().j_invariant() == E0.j_invariant()
    phi = T.curve().isomorphism_to(E0)
    T=phi(T)
    Tbis = KM2.elliptic_two_torsion(target=E0)
    assert T==Tbis
    PT = P+T
    PTem2 = convert(PT)
    PTm2 = Pm2.translate()
    assert PTm2 == PTem2
    assert PTm2 == Pm2.translate_T2()

    T3=KM2.T3()
    T3e = T3.lift_to_elliptic_point(target=E0)
    assert Pm2.translate_T3() == convert(P+T3e)

    test_double_diff_add(convert, P, Q)
    test_ladder(convert, P, Q)

    print(f"Testing hybrid double")
    PPm2 = Pm2.double()
    PPTm2 = Pm2.hybrid_double()
    PPTm2b = PPm2.translate()
    assert PPTm2 == PPTm2b

    print(f"Testing hybrid diff add")
    PQm2 = Pm2.diff_add(Qm2, PmQm2)
    PmQTm2 = PmQm2.translate()
    PQTm2 = Pm2.hybrid_diff_add(Qm2, PmQTm2)
    PQTm2b = PQm2.translate()
    assert PQTm2 == PQTm2b

    print(f"Testing translated ladder")
    for scalar in scalar_tests:
        print(f"Testing translated ladder for l={scalar}")
        #lPm2 = Pm2.ladder(scalar)
        lPm2tr = Pm2.translated_ladder(scalar)
        lPm2trbis = convert(scalar*(P+T)+T)
        assert lPm2trbis == lPm2tr

    print(f"Testing hybrid ladder")
    for scalar in scalar_tests:
        print(f"Testing hybrid ladder for l={scalar}")
        lPm2, lPPm2 = Pm2.full_hybrid_ladder(scalar)
        lPm2bis, lPPm2bis = Pm2.full_ladder(scalar)
        assert lPm2 == lPm2bis
        assert lPPm2 == lPPm2bis

    print("============= Twisted'Theta model =============")
    MtT = TwistedHThetaLine.from_curve(E0)
    KT = MtT.codomain()
    reduce = KT.reduce_from_elliptic()
    Pt = MtT(P)
    Qt = MtT(Q)
    PmQt = MtT(P-Q)

    test_lifting(MtT, Pt)

    Pe = Pt.lift_to_elliptic_point()
    print(f"Testing translation")
    T1=KT.T1()
    T1e = T1.lift_to_elliptic_point()
    assert Pt.translate_T1() == reduce(Pe+T1e)
    T2=KT.T2()
    T2e = T2.lift_to_elliptic_point()
    assert Pt.translate_T2() == reduce(Pe+T2e)
    T3=KT.T3()
    T3e = T3.lift_to_elliptic_point()
    assert Pt.translate_T3() == reduce(Pe+T3e)

    test_double_diff_add(MtT, P, Q)
    test_ladder(MtT, P, Q)


proof.all(False)

ea=604
eb=363

A = ZZ(2**ea)
B = ZZ(3**eb)
p = A * B - 1
F = GF(p**2, name="i", modulus=[1, 0, 1])
i = F.gen()
Estart = EllipticCurve(F, [1, 0])
E0 = Estart

print(f"============= Arithmetic on {E0} =============")
P = E0.random_point()
Q = E0.random_point()
# P = A*P
test_arithmetic(E0, P, Q)

print("=========== Testing a new curve via a random isogeny =========")
# T = 2**(ea-100)*B*Estart.random_point()
# phistart = Estart.isogeny(T, algorithm="factored")
T = 2**(ea-8)*B*Estart.random_point()
phistart = Estart.isogeny(T)

E0 = phistart.codomain()
print(f"=> Tests on {E0}, j={E0.j_invariant()}")
P = E0.random_point()
Q = E0.random_point()
# P = A*P
test_arithmetic(E0, P, Q)
