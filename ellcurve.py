class Curve:
    def __init__(self, curve = None, base_ring=None):
        self._point = CurvePoint
        self._type = "GenericCurve"
        self._curve = curve
        self._base_ring = base_ring

    def __call__(self, coords):
        return self._point(self, coords)

    def __repr__(self):
        return f"{self._type} {self.curve()}"

    def zero(self):
        if self._curve:
            return self._curve(0)

    def base_ring(self):
        if self._base_ring is None:
            return self.curve().base_ring()
        return self._base_ring

    def curve(self):
        return self._curve

    def j_invariant(self):
        return self.curve().j_invariant()

    def a_invariants(self):
        return self.curve().a_invariants()
    def a1(self):
        return self.a_invariants()[0]
    def a2(self):
        return self.a_invariants()[1]
    def a3(self):
        return self.a_invariants()[2]
    def a4(self):
        return self.a_invariants()[3]
    def a6(self):
        return self.a_invariants()[4]

class CurvePoint:
    def __init__(self, parent, coords):
        if not isinstance(parent, Curve):
            raise ValueError

        self._parent = parent
        self._coords = tuple(coords)

    def __getitem__(self, item):
        return self._coords[item]

    def __setitem__(self, key, item):
        self._coords[key] = item

    def __repr__(self):
        return f'Point {self.coords()} on {self.parent()}'

    def base_ring(self):
        #return self.coords()[0].base_ring()
        return self.coords()[0].parent()

    def parent(self):
        """ """
        return self._parent

    def zero(self):
        return self.parent().zero()

    def is_zero(self):
        return self == self.zero()

    def coords(self):
        return self._coords

class EllCurvePoint(CurvePoint):
    def __init__(self, parent, coords):
        coords = tuple(coords)
        if len(coords)==2:
            coords=coords+tuple([1])
        super().__init__(parent, coords)

    def is_zero(self):
        #print(f"Is zero: {self} => {self.X == 0 and self.Z == 0}")
        return self.X() == 0 and self.Z() == 0

    def xy(self):
        X, Y, Z = self.XYZ()
        return (X/Z, Y/Z)

    def XYZ(self):
        return self.coords()

    def X(self):
        X, Y, Z = self.coords()
        return X

    def Y(self):
        X, Y, Z = self.coords()
        return Y

    def Z(self):
        X, Y, Z = self.coords()
        return Z

