# Kummer surface in θ^2 coordinates

def proj_equal(P,Q):
    (x1,y1,z1,t1)=P
    (x2,y2,z2,t2)=Q
    return (y1/x1 == y2/x2 and z1/x1 == z2/x2 and t1/x1 == t2/x2)

def hadamard(P):
    (x,y,z,t)=P
    # a recursive implementation would save some additions here
    return (x+y+z+t, x+y-z-t, x-y+z-t, x-y-z+t)

def square(P):
    (x,y,z,t)=P
    return (x*x, y*y, z*z, t*t)

def mult(P,P0):
    (x,y,z,t)=P
    (x0,y0,z0,t0)=P0
    return (x*x0, y*y0, z*z0, t*t0)

def quo(P,P0):
    (x,y,z,t)=P
    (x0,y0,z0,t0)=P0
    return (x/x0, y/y0, z/z0, t/t0)

def inv(P):
    (x,y,z,t)=P
    return (1/x, 1/y, 1/z, 1/t)

# 4M+4S+3m+3m0
def diff_add(P,Q,PmQ,P0):
    P0b=hadamard(P0) # precomputation
    (x,y,z,t)=PmQ
    P=hadamard(P)
    Q=hadamard(Q)
    R=mult(P,Q)
    R=quo(R, P0b) #Total: 4M+3m0
    R=hadamard(R)
    R=square(R) #4S
    return quo(R,PmQ) #3m

# 8S+3m+3m0; or 3M+5S+3m0 if we share the x2/AA and so on
def double(P,P0):
    return diff_add(P,P,P0,P0)

# 7M+9S+3m+6m0 by bit
def ladder(n, P, P0):
    n = abs(n)
    if n == 1:
        return P
    P1, P2 = P, double(P, P0)
    for bit in bin(n)[3:]:
        Q = diff_add(P2, P1, P, P0)
        if bit == "1":
            P2 = double(P2,P0)
            P1 = Q
        else:
            P1 = double(P1,P0)
            P2 = Q
    return P1

# 4S+3m0
def isogeny(P, P0):
    P=square(P) #4S
    P=quo(P,P0) #3m0
    P=hadamard(P)
    return P

# we have φ(P) and want 2P in θtw coord
# 4S+3m0
def halfdouble(P, P0b): #halfdouble := isogeny
    return isogeny(P, P0b)

# we have φ(P), φ(Q), P-Q and want P+Q in θtw' coord
# 7M
def halfdiffadd(P,Q,PmQ):
    R=mult(P,Q) #4M
    R=hadamard(R)
    return quo(R,PmQ) #3M

# 12M+4S+3m0 by bit for the precomputation (depending only on P)
# 7M+4S+3m0 by bit for the computation of n.P
def half_ladder(n, P, P0):
    n=abs(n)
    P0b=hadamard(P0)
    R=P

    # precomputation
    r=[R]
    for i in range(0, len(bin(n)[2:])):
        # 4S+3m0; +12M to compute the inverse of the coordinates of the normalised R
        if i%2 == 0:
            R=isogeny(R,P0)
        else:
            R=isogeny(R,P0b)
        r.append(R)
    P2=R #nnP
    F=P2[0].parent()
    P1=(F(1),F(1),F(1),F(1)) #nP
    prev="1"

    # ladder computation
    # at each step, either P1 is in θtw and P2 in θtw', or the reverse
    # Here θtw means θi(P) θi(0), while θtw' means θi(P) / θi(0)
    for i in range(0, len(r)-1):
        PmQ=r[-i-2]
        P1P2=halfdiffadd(P2,P1,PmQ)
        b=bin(n)[2+i]
        if b=="1":
            if prev=="0": #P2 was the point with the diff add and P1 with the double, hence P2 in θtw', P1 is in θtw
                if (len(r)-i+1)%2==0:
                    zero=inv(P0) #this is precomputed
                else:
                    zero=inv(P0b) #idem
            else:
                if (len(r)-i+1)%2==0: #P1 in θtw', P2 in θtw
                    zero=P0
                else:
                    zero=P0b
            P1=P1P2
            P2=halfdouble(P2,zero)
        else:
            if prev=="0": #P1 in θtw, P2 in θtw'
                if (len(r)-i+1)%2==0:
                    zero=P0
                else:
                    zero=P0b
            else: #P1 in θtw', P2 in θtw
                if (len(r)-i+1)%2==0: 
                    zero=inv(P0) #idem
                else:
                    zero=inv(P0b) #idem
            P2=P1P2
            P1=halfdouble(P1,zero)
        prev=b

    if prev=="1": #we need to go from θtw' to θtw
        P1=mult(P1,P0)
    return P1

# half ladder expect points in θtw
# while ladder expect points in θ^2
# half_ladder_w take points in θ^2; convert them to θtw, apply half_ladder, and then convert them back
# We have θtw(A:B:C:D) = θ^2(a:b:c:d)
def half_ladder_w(n, P, P0):
    P0b=hadamard(P0)
    P=hadamard(P)
    R=half_ladder(n, P, P0b)
    return hadamard(R)

if __name__ == "__main__" and "__file__" in globals():
    import time
    from sage.all import ZZ, GF, Sequence, randint

    # Parameters taken from Fast Cryptography in Genus 2,
    p=2**127-1
    F=GF(p)
    P0=Sequence([-11,22,19,3], universe=F) #theta null point
    # cardinal: 16r, r=809251394333065553414675955050290598923508843635941313077767297801179626051
    Pbis=Sequence([1,1,1,78525529738642755703105688163803666634], universe=F) #order 2 r
    P=double(Pbis,P0) #order r
    #(24325351206784912477557749967275141112, 127893552214151499988840297862185506089, 8794169240442668057944008211936069832, 6421668111180308495551136417318373524)

    n=104780840913075035701570139470139147319047038430487
    nP=ladder(n,P,P0)
    print(nP)

    nPh=half_ladder_w(n,P,P0)
    print(nPh)

    assert proj_equal(nP, nPh)

    card=809251394333065553414675955050290598923508843635941313077767297801179626051
    for i in range(1,100):
        n=randint(0, card)
        nP=ladder(n,P,P0)
        nPh=half_ladder_w(n,P,P0)
        assert proj_equal(nP, nPh)
