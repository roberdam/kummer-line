from ellcurve import Curve, EllCurvePoint
from kummer import StandardKummerLine, StandardKummerPoint
from morphism import Automorphism, ChangeModel, Isogeny, Translation, LinearChangeModel, TrivialChangeModel, Isomorphism
from sage.all import cached_method, PolynomialRing, EllipticCurve, matrix
from sage.schemes.elliptic_curves.ell_generic import EllipticCurve_generic
from sage.schemes.elliptic_curves.ell_point import EllipticCurvePoint_field

class MontgomeryCurve(Curve):
    @staticmethod
    def is_montgomery(E):
        # if E.__class__ == MontgomeryCurve:
        #     return true
        ainvs = E.a_invariants()
        A = ainvs[1]
        return ainvs == (0, A, 0, 1, 0)

    @staticmethod
    def is_short_weierstrass(E):
        ainvs = E.a_invariants()
        return ainvs[0]==0 and ainvs[2]==0 and ainvs[1]==0

    def __init__(self, curve, B=1, **kwds):
        if isinstance(curve, EllipticCurve_generic):
            if not MontgomeryCurve.is_montgomery(curve):
                raise ValueError('Must use Montgomery model')
            ainvs = curve.a_invariants()
            A = ainvs[1]
        else:
            A=curve
            curve=None
        super().__init__(curve=curve, **kwds)
        self._point = FullMontgomeryPoint
        self._type = "Montgomery Curve"
        self._B=B
        self._A=A

    def __repr__(self):
        return f"{self._type} A={self.A()} B={self.B()}"

    def A(self):
        return self._A

    def B(self):
        return self._B

    # warning: this does not handle the twist
    def curve(self):
        if self._curve is None:
            return EllipticCurve([0,self.A(),0,1,0])
        return self._curve

    def to_short_weierstrass(self, target=None):
        F = self.base_ring()
        A = self.A()
        B = self.B()

        A_sqr  = A*A
        A_cube = A*A_sqr
        a = 1 - A_sqr/3
        b = (2*A_cube - 9*A) / 27
        E=EllipticCurve(F, [a/B**2,b/B**3])
        def image(P):
            if P.is_zero():
                return E(0)
            x, y = P.xy()
            u =  (x+A/3)/B
            v = y/B
            return E(u, v)
        def inverse_image(P):
            if P.is_zero():
                return self.zero()
            x=P[0]/P[2]
            y=P[1]/P[2]
            v=y*B
            u=x*B-A/3
            return self(u,v)
        phi=ChangeModel(domain=self, codomain=E, image=image, inverse_image=inverse_image)
        if not target is None:
            psi=Isomorphism.isomorphism(E, target)
            return phi.compose(psi)
        return phi

    def montgomery_line(self, cls=None):
        if cls is None:
            cls=MontgomeryLine
        return cls((self.A(),1), B=self.B())

    def to_montgomery_line(self, cls=None):
        K=self.montgomery_line(cls=cls)
        lift = K.lift_to_montgomery(E=self)
        return ChangeModel(domain=self, codomain=K, image=K, inverse_image=lift)

    @classmethod
    def from_four_torsion(cls, T):
        E=T.curve()
        TT = T+T
        alpha = TT[0]
        beta = T[0] - alpha
        return cls.from_alpha_beta(E, alpha, beta)

    @classmethod
    def from_two_torsion(cls, T):
        E=T.curve()
        alpha = T[0]
        a=E.a4()
        beta = (3*alpha**2+a).sqrt()
        return cls.from_alpha_beta(E, alpha, beta)

    # T=(α,0) is a point of order 2
    # which satisfy 3α^2+a=β^2
    # Then (x,y) -> ((x-α)/β, y/β) sends
    # y^2:x^3+ax+b to B y^2: x^3+Ax^2+x
    # with A=3α/β and B=1/β
    @classmethod
    def from_alpha_beta(cls, E, alpha, beta):
        A=3*alpha/beta
        B=1/beta
        M=cls(A, B=B)
        def image(P):
            if P.is_zero():
                zero=P[2] #0
                R=zero.parent()
                coords = [zero, R(1), zero]
                return M(coords)
            x=P[0]; y=P[1]
            return M(((x-alpha)/beta, y/beta))
        def inverse_image(P):
            if P.is_zero():
                return E(P.coords())
            x=P.x(); y=P.y()
            return E(x*beta+alpha, y*beta)
        return ChangeModel(domain=E, codomain=M, image=image, inverse_image=inverse_image)

    @classmethod
    def from_curve(cls, E):
        assert MontgomeryCurve.is_short_weierstrass(E)
        a=E.a4()
        b=E.a6()
        Fx=PolynomialRing(E.base_ring(), name="x")
        x=Fx.gen()
        P=x**3+a*x+b
        alpha = P.roots()[0][0]
        T=E(alpha,0)
        return cls.from_two_torsion(T)

class FullMontgomeryPoint(EllCurvePoint):
    def to_short_weierstrass(self, target=None):
        f=self.parent().to_short_weierstrass()
        return f(self)

class MontgomeryLine(StandardKummerLine):
    @classmethod
    def from_montgomery_curve(cls, E):
        if not MontgomeryCurve.is_montgomery(E):
            raise ValueError('Must use Montgomery model')
        ainvs = E.a_invariants()
        A, C = ainvs[1], 1
        return cls((A, C), curve=E)

    @classmethod
    def from_elliptic_curve(cls, E):
        return cls.from_montgomery_curve(E)

    @classmethod
    def from_curve(cls, E):
        if isinstance(E, MontgomeryLine):
            return TrivialChangeModel.identity(E)
        if isinstance(E, MontgomeryCurve):
            return E.to_montgomery_line(cls=cls)
        if isinstance(E, StandardKummerLine):
            assert MontgomeryCurve.is_short_weierstrass(E.curve())
            a=E.a4()
            b=E.a6()
            Fx=PolynomialRing(E.base_ring(), name="x")
            x=Fx.gen()
            P=x**3+a*x+b
            alpha = P.roots()[0][0]
            T=E((alpha,1))
            return cls.from_two_torsion(T)
        assert isinstance(E, EllipticCurve_generic)
        if MontgomeryCurve.is_montgomery(E):
            M=MontgomeryCurve(E)
            return M.to_montgomery_line(cls=cls)
            #K=cls.from_montgomery_curve(E)
            #return ChangeModel(domain=E, codomain=K, image=K)
        fM=MontgomeryCurve.from_curve(E)
        p=fM.codomain().to_montgomery_line(cls=cls)
        return fM.compose(p)

    @classmethod
    def from_four_torsion(cls, T, TT = None):
        if isinstance(T, EllipticCurvePoint_field):
            TT=T+T
            #x=T[0]; z=T[2]
            #u=TT[0]; w=TT[2]
            p=StandardKummerLine.to_kummer_line(T.curve())
            K=p.codomain()
            return p.compose(cls.from_four_torsion(K(T), K(TT)))
        # x -> (x-x0)/β, α=x0=u/w, β=x/z-x0=(xw-uz)/wz
        # (X:Z)->(X/Z-u/w)/β=((Xw-uZ)/wZ:(xw-uz)/wz)=((Xw-uZ)z:(xw-uz)Z)
        # (X:Z)->(zwX-zuZ:(xw-zu)Z)
        # And the Montgomery coefficient is A=3α/β=3zu/(wx-zu)
        if TT is None:
            TT = T.double()
        x, z=T.coords()
        u, w=TT.coords()
        K = T.parent()
        alpha = u/w
        beta = x/z-alpha
        A, C = 3*alpha/beta, 1
        codomain=cls((A, C))
        #K=StandardKummerLine(constants=E.a_invariants(), curve=E)
        N = matrix([[z*w,-z*u],[0,x*w-z*u]])
        f = LinearChangeModel(domain=K, codomain=codomain, N=N)
        return f

    @classmethod
    def from_two_torsion(cls, T):
        if isinstance(T, EllipticCurvePoint_field):
            p=StandardKummerLine.to_kummer_line(T.curve())
            return p.compose(cls.from_two_torsion(p.codomain()(T)))
        u,v = T.coords()
        K = T.parent()
        a = K.curve().a4()
        alpha = u/v
        beta = (3*alpha**2+a).sqrt()
        return cls.from_alpha_beta(K, alpha, beta)

    @classmethod
    def from_alpha_beta(cls, K, alpha, beta):
        if isinstance(K, EllipticCurve_generic):
            p=StandardKummerLine.to_kummer_line(K)
            return p.compose(cls.from_alpha_beta(p.codomain(), alpha, beta))
        A, C = 3*alpha/beta, 1
        codomain=cls((A, C))
        N = matrix([[1,-alpha],[0,beta]])
        f = LinearChangeModel(domain=K, codomain=codomain, N=N)
        return f

    def __init__(self, constants, B=1, **kwds):
        A, C = constants
        R=A.parent(); C=R(C)
        super().__init__(constants=(A,C), **kwds)
        self._B=R(B)
        self._point = MontgomeryPoint
        self._type = "Montgomery"

    def B(self):
        return self._B

    def AC(self):
        return self.constants()

    def A(self):
        A, C=self.AC()
        return A
    def C(self):
        A, C=self.AC()
        return C

    def A24(self):
        A, C=self.AC()
        return (A+2*C)/(4*C)

    def A24proj(self):
        A, C=self.AC()
        return (A+2*C), (4*C)

    def T1(self):
        R=self.base_ring()
        return self((R(0),R(1)))
    def TT1(self):
        R=self.base_ring()
        return self((R(1),R(1)))
    def TT1bis(self):
        R=self.base_ring()
        return self((R(-1),R(1)))
    def translate_T1(self):
        N=matrix([[0, 1], [1, 0]])
        return Translation(self, N=N)

    def translate_by(self, T):
        A,B=T.coords()
        if B==0:
            return self
        if A==0:
            return self.translate_T1()
        N = matrix([[A,-B],[B,-A]])
        return Translation(self, N=N)

    def curve(self):
        if self._curve is None:
            return self.weierstrass_curve()
        return super().curve()

    def montgomery_curve(self, cls=None):
        if cls is None:
            cls=MontgomeryCurve
        return cls(self.a(), B=self.B())

    # warning: this does not handle the twist
    @cached_method
    def weierstrass_curve(self):
        F = self.base_ring()
        a = self.a()
        return EllipticCurve(F, [0,a,0,1,0])

    def elliptic_curve(self):
        return self.weierstrass_curve()

    @cached_method
    def j_invariant(self):
        """
        Compute the j-invariant of the Kummer Line
        """
        A, C = self.AC()
        j_num = 256 * (A**2 - 3*C**2)**3
        j_den = C**4 * (A**2 - 4*C**2)
        return j_num / j_den

    @cached_method
    def a(self):
        """
        Compute the Montgomery coefficient as a value
        in the base field
        """
        A, C = self.AC()
        return A / C

    def find_T2(self):
        A = self.a()
        d = (A * A - 4).sqrt()
        alpha = (-A + d) / 2
        return self((alpha, 1))

    def montgomery2(self, T2=None, cls=None):
        from montgomery2 import Montgomery2Line
        if cls is None:
            cls=Montgomery2Line
        if T2 is None:
            T2=self.find_T2()
        return cls(tuple(T2))

    def to_montgomery2(self, T2=None, cls=None):
        M2 = self.montgomery2(T2, cls=cls)
        return TrivialChangeModel(domain=self, codomain=M2)

    #@cached_method
    def to_theta_null_point(self, cls=None):
        from theta import ThetaLine
        if cls is None:
            cls = ThetaLine
        A = self.a()

        # alpha is a root of x^2 + Ax + 1
        disc = A * A - 4
        assert disc.is_square()

        d = (A * A - 4).sqrt()
        alpha = (-A + d) / 2

        # The theta coordinates a^2 and b^2
        # are related to alpha
        aa = alpha + 1
        bb = alpha - 1

        aabb = aa * bb
        ab = aabb.sqrt()

        # We aren't given (a,b) rational, but (a/b) is rational so we use (ab : b^2) as the theta null point
        O0 = cls((ab, bb))
        return O0

    #@cached_method
    def to_theta(self, th=None):
        from theta import ThetaLine
        if not isinstance(th, ThetaLine):
            th = self.to_theta_null_point(cls=th)
        a, b = th.coords()
        N = matrix([[a,-a],[b,b]])
        f = LinearChangeModel(domain=self, codomain=th, N=N)
        return f

    def to_short_weierstrass_line(self):
        F = self.base_ring()
        A = self.a()
        B = self.B()

        A_sqr  = A*A
        A_cube = A*A_sqr
        a = 1 - A_sqr/3
        b = (2*A_cube - 9*A) / 27
        E=EllipticCurve(F, [a/B**2,b/B**3])
        K=StandardKummerLine(E)
        #def image(P):
        #    x, z = P.XZ()
        #    A,C = self.AC()
        #    u =  3*C*x+A*z
        #    v = 3*C*z*B
        #    return K((u, v))
        A,C = self.AC()
        N = matrix([[3*C,A],[0,3*C*B]])
        return LinearChangeModel(domain=self, codomain=K, N=N)

    def to_kummer_line(self, target=None):
        f=self.to_short_weierstrass_line()
        if target is None:
            return f
        K=f.codomain()
        g=K.isomorphism_to(target)
        return f.compose(g)

    def lift_to_montgomery(self, E=None, cls=None):
        if E is None:
            E = self.montgomery_curve(cls=cls)
        A = self.a()
        B = self.B()
        def image(P):
            # Compute y2, assume x is a valid coordinate
            x = P.x()
            y2 = (x * (x**2 + A*x + 1))/B
            y = y2.sqrt()
            return E((x, y))
        return ChangeModel(domain=self, codomain=E, image=image, inverse_image = self)

    # unlike Kummer.lift_to_elliptic, we take into account the twist 'B'
    def lift_to_short_weierstrass(self, target=None):
        f1 = self.lift_to_montgomery()
        montgomery = f1.codomain()
        f2 = montgomery.to_short_weierstrass(target=target)
        return f1.compose(f2)

    @cached_method
    def translate_T1(self):
        N = matrix([[0,1],[1,0]])
        return Translation(domain=self, N=N)

    #Tb is a point of 8-torsion above TT1
    def isogeny_T1(self, Tb=None):
        from montgomery2 import Montgomery2Line
        if Tb is None:
            pass
        r,s=Tb
        codomain = Montgomery2Line(((r-s)**2, -4*r*s))
        # on the codomain, (A+2)/4=-(x-z)^2/4xz
        # with x=(r-s)**2, z=-4rs, (A+2)/4= (r+s)^4/4(r-s)^2((r+s)^2-(r-s)^2))
        # the constant γ is computed in 2S+3a
        # and then (A+2)/4 for the arithmetic in 2S+3a
        def image(P):
            x, z = P
            # can be computed in 2S+2m0+3a
            X = (4*r*s)*(x-z)**2
            Z = (4*x*z)*(r-s)**2
            return (X, Z)
        return Isogeny(domain=self, codomain=codomain, image=image)
    #If we start from an arbitrary model, and we have Tb
    #we compute TT=2Tb=(x:z) and T=(u:w)
    #we can map TT, T to Montgomery position (1:1), (0:1) via
    #(X:Z)->(zwX-zuZ:(xw-zu)Z)
    #cf `from_four_torsion`
    #if we are on a Montgomery curve already, and T!=(0,1)
    #then (x:z)=(a':b'), (u:v)=(A^2:B^2)=(a'^2+b'^2: 2a'b')
    #and the transformation is (X',Z')=(2a' X - (a'^2+b'^2) Z: (a'^2-b'^2) Z)
    #and then composing with part of the isogeny above gives:
    #((X'-Z')^2: 4X'Z')=(a'^2 X^2 -2a'^3 XZ +a'^4 Z^2 : 2(a'^3 - a'b'^2) XZ +(b'^4-a'^2) Z^2)

    def to_affine(self):
        return TrivialChangeModel(self, AffineMontgomeryLine(self))

class MontgomeryPoint(StandardKummerPoint):
    def __init__(self, parent, coords):
        if isinstance(coords, FullMontgomeryPoint):
            if coords.is_zero():
                zero=coords[2] #0
                R=zero.parent()
                coords = [R(1), zero]
            else:
                coords = coords.coords()
                coords = [coords[0], coords[2]]
        super().__init__(parent, coords)

    # unlike lift_to_elliptic_point, we take the twist into account
    def lift_to_short_weierstrass_point(self, target=None):
        return self.parent().lift_to_short_weierstrass()(self, target=target)

    # Cost: 2S+2M+2m0+4a; 2S+2M+1m0 if C=1
    def _double(self):
        X, Z = self.XZ()
        A, C = self._parent.AC()
        # X2, Z2 = self.xDBL(X, Z, A, C)
        # k1/k2 = (a+2)/4
        k1 = A+2*C
        k2 = 4*C
        a=(X+Z)**2
        b=(X-Z)**2
        c=a-b
        X2=a*b*k2
        Z2=c*(b*k2+k1*c)

        return self.parent()((X2, Z2))

    def double(self):
        if self.is_zero():
            return self
        return self._double()

    # Cost: 2S+2M+2m+6a [1m if P-Q normalised]
    def _diff_add(self, Q, PmQ):
        XP, ZP = self.XZ()
        XQ, ZQ = Q.XZ()
        XPmQ, ZPmQ = PmQ.XZ()
        #XPQ, ZPQ = self.xADD(XP, ZP, XQ, ZQ, XPmQ, ZPmQ)
        a=XP+ZP
        b=XP-ZP
        c=XQ+ZQ
        d=XQ-ZQ
        XPQ=(d*a+c*b)**2*ZPmQ
        ZPQ=(d*a-c*b)**2*XPmQ

        return self.parent()((XPQ, ZPQ))

    def diff_add(self, Q, PmQ):
        if self.is_zero():
            return Q
        if Q.is_zero():
            return self
        if PmQ.is_zero():
            return self._double()

        return self._diff_add(Q, PmQ)

class AffineMontgomeryLine(MontgomeryLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = AffineMontgomeryPoint
        self._type = "AffineMontgomery"

    def zero(self):
        R=self.base_ring()
        return self((self.C(), R(0)))

    def to_projective(self):
        return TrivialChangeModel(self, MontgomeryLine(self))

class AffineMontgomeryPoint(MontgomeryPoint):
    # Cost: 2S+2M+2m0+4a; 2S+2M+1m0 if C=1
    def double(self):
        X, Z = self.XZ()
        A, C = self.parent().AC()
        # k1/k2 = (a+2)/4
        #k1 = A+2*C
        #k2 = 4*C
        k1=A+2*C
        k2=4*C
        a=(X+Z)**2
        b=(X-Z)**2
        c=a-b
        X2=a*b*k2
        Z2=c*(b*k2+k1*c)
        return self.parent()((X2, Z2))

    # Cost: 2S+2M+2m+6a [1m if P-Q normalised]
    def diff_add(self, Q, PmQ):
        if PmQ.is_zero():
            l1=self.ratio(Q)
            l2=self.zero().ratio(PmQ)
            scale=l1*l1/l2
            print(f"Detecting doubling; scaling by {scale}")
            return self.double().scale(scale)
        XP, ZP = self.XZ()
        XQ, ZQ = Q.XZ()
        XPmQ, ZPmQ = PmQ.XZ()
        a=XP+ZP
        b=XP-ZP
        c=XQ+ZQ
        d=XQ-ZQ
        XPQ=(d*a+c*b)**2/XPmQ
        ZPQ=(d*a-c*b)**2/ZPmQ

        return self.parent()((XPQ, ZPQ))

# This is exactly like MontgomeryLine (ie particular we don't divide)
# except there is no fancy detection of whether we do a doubling inside the
# diff add
# so that we get as close as possible to the actual implementation of
# Montgomery curves in DH exchange
class Affine2MontgomeryLine(AffineMontgomeryLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = Affine2MontgomeryPoint
        self._type = "Affine2Montgomery"

class Affine2MontgomeryPoint(MontgomeryPoint):
    def double(self):
        X, Z = self.XZ()
        A, C = self.parent().AC()
        k1=A+2*C
        k2=4*C
        a=(X+Z)**2
        b=(X-Z)**2
        c=a-b
        X2=a*b*k2
        Z2=c*(b*k2+k1*c)
        return self.parent()((X2, Z2))

    # Cost: 2S+2M+2m+6a [1m if P-Q normalised]
    def diff_add(self, Q, PmQ):
        XP, ZP = self.XZ()
        XQ, ZQ = Q.XZ()
        XPmQ, ZPmQ = PmQ.XZ()
        a=XP+ZP
        b=XP-ZP
        c=XQ+ZQ
        d=XQ-ZQ
        XPQ=(d*a+c*b)**2*ZPmQ
        ZPQ=(d*a-c*b)**2*XPmQ
        return self.parent()((XPQ, ZPQ))

class CubicalMontgomeryLine(AffineMontgomeryLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = CubicalMontgomeryPoint
        self._type = "CubicalMontgomery"

    #def zero(self):
    #    R=self.base_ring()
    #    return self((R(4)*self.C(), R(0)))

class CubicalMontgomeryPoint(MontgomeryPoint):
    #4x_[2]P x_P (x^2_P + Ax_P + 1) = (x^2_P − 1)^2
    def double(self):
        #x = self.x()
        #z = self.Z()
        A = self.parent().a()
        # xx = (x**2-1)**2/(4*x*(x**2+A*x+1))
        # zz = z**4*(x*(x**2+A*x+1))
        # return self.parent()((xx*zz, zz))
        ## Simplifications:
        # xxzz = (x**2-1)**2*z**4/4 = (X^2-Z^2)^2/4
        # zz = z**4*(x*(x**2+A*x+1))
        # return self.parent()((xxzz, zz))
        X = self.X()
        Z = self.Z()
        XX = (X**2-Z**2)**2
        ZZ = 4*(Z*X**3 + A*Z**2*X**2+Z**3*X)
        return self.parent()((XX, ZZ))
    #for a double, MontgomeryLine/AffineMontgomeryLine is off by a factor x4

    #x_{P+Q}x{P-Q}(xP-xQ)^2 = (xPxQ-1)^2
    def diff_add(self, Q, PmQ):
        # xP = self.x()
        # zP = self.Z()
        # xQ = Q.x()
        # zQ = Q.Z()
        # xPmQ = PmQ.x()
        # zPmQ = PmQ.Z()
        # X=(xP*xQ-1)**2/((xP-xQ)**2*xPmQ)
        # Z=zP**2 * zQ**2 * (xP-xQ)**2 / zPmQ
        # return self.parent()((X*Z, Z))
        # # simplifications:
        # XZ = ZP^2 ZQ^2 (xP xQ -1)**2/ (xPmQ ZPmQ)
        #    = (XP XQ-ZP ZQ)^2 / XPmQ
        # Z = (ZQ XP - ZP XQ)^2 / ZPmQ
        XP, ZP = self.XZ()
        XQ, ZQ = Q.XZ()
        XPmQ, ZPmQ = PmQ.XZ()
        XX = (XP*XQ-ZP*ZQ)**2 / XPmQ
        ZZ = (ZQ*XP-ZP*XQ)**2 / ZPmQ
        return self.parent()((XX, ZZ))
    # for a diff add, AffineMontgomeryLine is off by a factor x4
    # while MontgomeryLine is off by a factor 4 Z(P-Q)/X(P-Q)
    # ie for a ladder, it is off by 4/x(P)

