# special doubling formula that goes with the new 'doubleadd' trick
def double(nP, P, Q, r, prevbit="0"):
    r=r*r
    if prevbit == "1":
        nP=nP-P
    nnP, s = miller_double(nP, Q-P)
    return (nnP, r*s)

def doubleadd(nP, P, Q, r, prevbit="0"):
    r=r*r
    if prevbit == "0":
        nP=nP+P
    nnP, s = miller_double(nP, Q)
    return (nnP, r*s)

# the miller function μ_{nP,nP}(Q)
def miller_double(nP, Q):
    l = nP._line_(nP, Q)
    nnP = nP+nP
    v = nnP._line_(-nnP, Q)
    return (nnP, l/v)

# the miller function μ_{nP,P}(Q)
def miller_add(nP, P, Q):
    l = nP._line_(P, Q)
    nPP = nP+P
    v = nPP._line_(-nPP, Q)
    return (nPP, l/v)

def threeway(P1, P2, Q):
    l = P1._line_(P2, Q)
    d1 = Q[0]-P1[0]
    d2 = Q[0]-P2[0]
    return l/(d1*d2)

def ladder0(n, P, Q):
    nP=P; r=1
    for bit in bin(n)[3:]:
        nP, s = miller_double(nP, Q)
        r=r*r*s
        if bit == "1":
            nP, s = miller_add(nP, P, Q)
            r=r*s
    return r

# double and add exponentiation, using new DoubleAndAdd formula
def ladder1(n, P, Q):
    nP=P
    nnP, r=miller_double(nP, Q)

    if n==2:
        return (nP, r)

    # at each step, r = f_{(n+1)P}(Q)
    prevbit="0"
    for bit in bin(n-1)[3:]:
        if bit == "0":
            nP, r = double(nP, P, Q, r, prevbit=prevbit)
        else:
            nP, r = doubleadd(nP, P, Q, r, prevbit=prevbit)
        prevbit=bit

    return r

# double and add exponentiation, using threeway additions
def ladder2(n, P, Q):
    nP=P; r=1
    for bit in bin(n)[3:]:
        s = threeway(nP, nP, Q)
        nP=nP+nP
        r=r*r*s
        if bit == "1":
            s = threeway(nP, P, Q)
            nP=nP+P
            r=r*s
    return r

def tate0(n, P, Q, k=1, exp=ladder0):
    p=P.curve().base_ring().characteristic()
    assert (p**k-1)%n == 0
    d=(p**k-1)//n
    e = exp(n, P, Q)
    return e**d

def tate1(*a, **kws):
    return tate0(*a, **kws, exp=ladder1)
def tate2(*a, **kws):
    return tate0(*a, **kws, exp=ladder2)


if __name__ == "__main__" and "__file__" in globals():
    import time
    from sage.all import ZZ, GF, EllipticCurve, proof

    def testBN():
        p=16030569034403128277756688287498649515636838101184337499778392980116222246913;
        F=GF(p)
        E=EllipticCurve(F, [0,5])
        # E.j_invariant()=0, p=1 mod 4 so p splits in Q(i) and E ordinary
        r=E.cardinality() # this is prime: r.is_prime(): True
        #k=emb_degree(r,p) # 12
        k=12
        F2=GF(p**12, 't')
        E2=E.base_extend(F2)
        r2=E2.cardinality()

        #AC=(F2(6123150921968337422568796176365652246109592885288416056556751737102623758294), F2(1))
        #A24 = ((AC[0]/AC[1]+2)/4)
        #E2M=EllipticCurve(F2, [0,AC[0],0,1,0]) #this is a twist of E over Fp

        P=E.random_point()
        Q=(r2//r**2)*E2.random_point() # multiply by cofactor to get point of r-torsion
        print(f"- Test pairings on G1xG3, n={r}\n")
        #test_odd_degree_pairings(E2, r, r2//r**2, A24, k=k, points=[phi(E2(P)),phi(Q)])

        time0 = time.time()
        t0=E2(P).tate_pairing(Q, r, k=k)
        print(f"Standard Tate pairing computation: {time.time() - time0:.5f}")

        time0 = time.time()
        t1=tate0(r, E2(P), Q, k=k)
        print(f"Double And Add Miller ladder: {time.time() - time0:.5f}")
        time0 = time.time()
        t2=tate1(r, E2(P), Q, k=k)
        print(f"DoubleAndAdd Miller ladder: {time.time() - time0:.5f}")
        time0 = time.time()
        t3=tate2(r, E2(P), Q, k=k)
        print(f"ThreeWay Miller ladder: {time.time() - time0:.5f}")

        assert t0 == t1
        assert t0 == t2
        assert t0 == t3

    print("########### Testing a BN curve ############")
    testBN()
