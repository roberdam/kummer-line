This repository contains Sage codes for various formulas related to the arithmetic of Kummer lines: additions, isogenies, and pairings.

Most of these formulas are not published yet, but they are quickly described in the notes [Improving the arithmetic of Kummer lines](http://www.normalesup.org/~robert/pro/publications/notes/2023-11-kummer_lines.pdf), also available as [Chapter 8](http://www.normalesup.org/~robert/pro/publications/notes/notes_av.pdf) of the full notes.

For the arithmetic and isogenies, we mainly focus on models given by the Montgomery model, the Montgomery model with full rational 2-torsion, the theta models and some twisted variants of the theta model, along with change of models formulas between them. These models (and their conversions) are described in Section 3 of the notes.

For the arithmetic we implement doubling, differential additions, and also translated doublings (which can be more efficient than doublings), along with 2-isogenies (and radical 2-isogenies) and their images (or translated images).
These formulas are desdribed in Sections 4 and 5 of the notes. Part of these formulas are now published in [Computing 2-isogenies between Kummer lines](https://eprint.iacr.org/2024/037). 

This is implemented in the following files:
- `ellcurve.py`: generic class for curves (or Kummer lines) and points
- `kummer.py`: generic class for Kummer lines, along with projection from curves and lifting to curves.
- `morphism.py`: generic class for morphisms (isomorphisms, change of models, isogenies)
- `montgomery.py`: instanciation for the Montgomery models.
- `montgomery2.py`: instanciation for the Montgomery models with full rational two torsion.
- `theta.py`: instanciation fot theta models and twisted variants of theta models.

The Montgomery2 and theta models also have an implementation of a time/memory trade off for the Montgomery ladder described in Section 6 of the notes.

The pairings formula use the biextension arithmetic, as explained in the Section 7 of the notes and the slides [Arithmetic and pairings on Kummer lines](http://www.normalesup.org/~robert/pro/publications/slides/2023-10-Leuven.pdf), and the preprint [Fast pairings via biextensions and cubical arithmetic](https://eprint.iacr.org/2024/517).
We use the cubical torsor structure representation to work in the biextension, this is implemented in the `Affine*Line` classes of the models above.

- `biextension.py`: biextension arithmetic and resulting Weil and Tate
    pairings formulas
- `cubical.py`: cubical arithmetic for Weierstrass curves

The folder `pairings/` contain standalone implementations.
- `pairings/montgomery_simplified.py`: standalone file which implement the biextension arithmetic on Montgomery models and the Weil and Tate pairing (along with comparison with the default Sage implementation and the low level Pari call).
- `pairings/cubical_simplified.py`: cubical arithmetic implemented for an elliptic curve of the form `y^2=x^3+ a_2 x^2 + a_4 x + a_6`, and resulting pairing formulas for the Tate and Weil pairing. The formulas currently implemented use division and are not meant to be efficient, contrary to `montgomery_simplified`.

Along with these files, we also provide the following extra tools in `extras/`:
- `extras/dlp.py`: this gives an explicit example of a dlp recovery from a projective coordinate leak in the Montgomery ladder, by using the coordinate leak as monodromy information via the cubical torsor structure. See Section 7.2 of the notes for some explanations, and Section 6.4 of the biextension preprint.
- `extras/utils.py`: misc utilities, currently these implement optimal isogeny strategies
- `surface/gaudry_schost.py`: the half ladder implementation for the Gaudry-Schost Kummer surface. For a fixed point scalar multiplication, we do a precomputation of 12M+4S+3m0 by bit, and then each scalar multiplication costs 7M+4S+3m0 by bit. By contrast the usual ladder costs 10M+9S+6m0 by bit.

Test files can be run in `tests/`:
- `test/*.py`: test suites

Some more experimental files are not yet publicly available, but can be asked upon request.
