from kummer import KummerPoint
from montgomery import MontgomeryLine, MontgomeryPoint
from morphism import Automorphism, Isomorphism, ChangeModel, Isogeny, TranslatedIsogeny, TrivialChangeModel, Translation, LinearChangeModel
from sage.all import cached_method, EllipticCurve, matrix

# Montgomery + Legendre, represented by its two torsion point T_2 (which is not (0,1))
class Montgomery2Line(MontgomeryLine):
    @classmethod
    def from_montgomery_curve(cls, K):
        if not isinstance(K, MontgomeryLine):
            K=MontgomeryLine.from_montgomery_curve(K)
        A = K.a()

        # alpha is a root of x^2 + Ax + 1
        disc = A * A - 4
        assert disc.is_square()
        d = (A * A - 4).sqrt()
        alpha = (-A + d) / 2

        return cls((alpha,1))

    @classmethod
    def from_curve(cls, E):
        f=MontgomeryLine.from_curve(E)
        return f.compose(f.codomain().to_montgomery2(cls=cls))

    @classmethod
    def from_two_torsion(cls, T):
        return cls(T)

    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = Montgomery2Point
        self._type = "Montgomery2"

    def to_montgomery(self, cls=None):
        if cls is None:
            cls=MontgomeryLine
        K=cls(self.AC())
        return TrivialChangeModel(domain=self, codomain=K)

    def to_twistedhtheta(self, cls=None):
        from theta import TwistedHThetaLine
        if cls is None:
            cls=TwistedHThetaLine
        f=self.translate_T2()
        N=f.N()
        codomain=cls(self.two_torsion())
        return LinearChangeModel(domain=self, codomain=codomain, N=N)

    def translated_to_twistedhtheta(self, cls=None):
        from theta import TwistedHThetaLine
        if cls is None:
            cls=TwistedHThetaLine
        codomain=cls(self.two_torsion())
        return TrivialChangeModel(domain=self, codomain=codomain)

    # A=-(x/z + z/x) = -(x^2+z^2)/xz
    # (A+2)/4=-(x-z)^2/4xz
    @cached_method
    def AC(self):
        # Cost: 2S
        x, z = self.constants()
        # return (-(x*x+z*z)/(x*z), 1)
        return (x*x+z*z, -x*z)

    #NB: if self.T2()=(x:z), then self.j_invariant() is
    #(256*x^12 - 768*z^2*x^10 + 1536*z^4*x^8 - 1792*z^6*x^6 + 1536*z^8*x^4 - 768*z^10*x^2 + 256*z^12)/(z^4*x^8 - 2*z^6*x^6 + z^8*x^4)

    def two_torsion(self):
        return self.constants()

    def two_torsion_point(self):
        return self(self.two_torsion())

    def T2(self):
        return self.two_torsion_point()

    def T3(self):
        (A,B)=self.two_torsion_point()
        return self((B,A))

    def elliptic_two_torsion(self, target=None):
        if target is None:
            E=self.curve()
            x, z = self.two_torsion()
            return E((x,0,z))
        else:
            return self.two_torsion_point().lift_to_elliptic_point(target=target)

    @cached_method
    def translate_T2(self):
        A,B = self.T2()
        N = matrix([[A,-B],[B,-A]])
        return Translation(self, N=N)

    def translate_T3(self):
        A,B = self.T3()
        N = matrix([[A,-B],[B,-A]])
        return Translation(self, N=N)

    def translate(self):
        return self.translate_T2()

    def isogeny_T2(self, Tb=None, klass=MontgomeryLine):
        AA,BB = self.T2()
        if Tb is None:
            b=self.B()
            b2=b*AA/BB
            # Formula from Renes, eprint 2017/1198
            codomain = klass((2*(BB**2-2*AA**2), BB**2), B=b2)
            #on the codomain, a24 = (BB^2-AA^2: BB^2)
        else:
            #If we have a point of 4-torsion above T2, the codomain is still of Montgomery2 type
            #fT=self.translated_isogeny_T2(Tb)
            #return fT.compose(fT.codomain().translate_T2())
            ab, bb = Tb.coords()
            codomain = self.__class__((ab**2, bb**2))
        def image(P):
            x, z = P
            # Formula from Renes, eprint 2017/1198
            # can be computed in 2M+2m0+4a (+2a precomp)
            # via (xAA-zBB:xBB-zAA)=H((x-z)(AA+BB):(x+z)(AA-BB))
            # or in 2S+3m0+?a
            X = x*(x*AA-z*BB)
            Z = z*(x*BB-z*AA)
            return (X, Z)
        return Isogeny(domain=self, codomain=codomain, image=image)
    # For a 4-isogeny, see: https://eprint.iacr.org/2017/504.pdf
    # (A′_24 : C′_24) = (X4^4 − Z4^4 : Z4^4) -> 4S+4a
    # (X′ : Z′) = (X (2X4Z4 Z − (X4^2+Z4^2) X) (X4 X − Z4 Z)^2:
    #              Z (2X4Z4 X − (X4^2+Z4^2) Z) (Z4 X − X4 Z)^2)
    # -> 6M+2S+6a

    def radical_isogeny_T2(self, sign=1):
        AA,BB=self.T2()
        aa=AA+BB; bb=AA-BB
        ab=(aa*bb).sqrt()
        ab *= sign
        abis=aa+ab; bbis=aa-ab
        return self.isogeny_T2(Tb=self((abis, bbis)))

    # let Tb be above T2, return f(P+Tb)=f(P)+T'2
    # the isogeny f sends Tb to T'2, so iterating we get a 4-isogeny
    # The homography (x':z')=((x+z)/(a'+b'):(x-z)/(a'-b'))
    # sends TT2 to (1:1), 0 to (b:a), T1 to (b:-a)
    # T2 to (a:b), T3 to (-a:b)
    # so the image of T2 is of Montgomery type
    # hence (x'+z')^2 and (x'-z')^2 descends to the isogenous Kummer
    # computing the isogeneous ramification we see that we need to
    # translate by f(T'2) to sends 0 to infinity
    ## This is the same formula as doing a translated_conversion to twisted_thetah and applying isogeny_T2 and converting back via a translated_conversion
    def translated_isogeny_T2(self, Tb):
        ab, bb = Tb.coords()
        a=ab+bb; b=ab-bb
        codomain = Montgomery2Line((ab**2, bb**2))
        def image(P):
            x, z = P
            # can be computed in 2m0+2S+4a
            X = ((x+z)/a+(x-z)/b)**2
            Z = ((x+z)/a-(x-z)/b)**2
            return (X, Z)
        return TranslatedIsogeny(domain=self, codomain=codomain, image=image)
    # Applying the translation after, we get:
    # U=x^2*((4*ab^2 + 4*bb^2)/(ab^2 - bb^2)) + x*z*(8*bb*ab/(-ab^2 + bb^2))
    # V=x*z*(-8*bb*ab/(-ab^2 + bb^2)) + z^2*((4*ab^2 + 4*z^2*bb^2)/(-ab^2 + bb^2))
    # and we recover exactly the image formula above

    #return f(P+TT1)=f(P)+T'2
    #the isogeny f sends T2 to T'1, so the next isogeny is the dual isogeny and composing with it we get [2]
    ## This is not the same as doing a translated_conversion to twisted_thetah, applying isogeny_T1 and converting back via a translated_conversion; because the translation would be f(x+T2)+T'2=f(x)+T'1+T'2=f(x)+T'3. Instead we use for two torsion point T'3=(bb,aa) rather than T'2=(aa,bb), so that the end result is translation by T'2
    def translated_isogeny_T1(self):
        AA,BB = self.T2()
        aa=AA+BB
        bb=AA-BB
        codomain = Montgomery2Line((bb, aa)) #we use T'3 here!
        def image(P):
            x, z = P
            X = (x+z)**2/aa
            Z = (x-z)**2/bb
            return (X, Z)
        return TranslatedIsogeny(domain=self, codomain=codomain, image=image)

    def isogeny_T1(self, Tb=None):
        if not Tb is None:
            return super().isogeny_T1(Tb=Tb)
        f=self.translated_isogeny_T1()
        t=f.codomain().translate_T2()
        return f.compose(t)

class Montgomery2Point(MontgomeryPoint):
    # translating switches to θtw'=θ^2 coordinates
    # hybrid double and diff add then does the arithmetic in θtw'
    # translating back goes back to Montgomery coordinates
    #
    #`translate_T2` in KummerPoint yields the same function, cf the tests
    def translate(self):
        A, B = self.parent().two_torsion()
        x, z = self.XZ()
        X= A*x-B*z
        Z= B*x-A*z
        return self.parent()((X, Z))

    # given P, Q and P-Q+T returns P+Q+T
    # diff add costs: 2S+2M+2m [1m if P-Q normalised]
    # hybrid diff add costs: 2S+2M+2m+2m0+6a [1m0 if constants normalised, 1m if P-Q normalised]
    def hybrid_diff_add(self, Q, PmQ):
        P=self.to_twisted_tr()
        R=P.diff_add(Q.to_twisted_tr(), PmQ.to_twisted_tr())
        return R.to_montgomery2_tr()

    # return 2P+T
    # double costs: 2S+2M+2m0 [1m0 if constants normalised]
    # hybrid double cost: 4S+4m0+4a [2m0 if constants normalised]
    def hybrid_double(self):
        return self.hybrid_diff_add(self, self.parent().two_torsion_point())

    def translated_ladder(self, n):
        P=self.to_twisted_tr()
        R=P.ladder(n)
        return R.to_montgomery2_tr()

    # Montgomery ladder: 4S+4M+2m+2m0 [1m if P normalised, 1m0 if constants normalised]
    # Hybrid ladder: 6S+2M+2m+6m0 [1m if P normalised, 3m0 if constants normalised]
    # we want to use the hybrid double with the standard diff add
    def full_hybrid_ladder(self, n):
        n = abs(n)
        P = self
        Pt = P.translate()
        P1, P2 = P.parent().zero(), P

        if n == 0:
            return P1, P2

        toggle=False
        for bit in bin(n)[2:]:
            if toggle:
                Q = P2.diff_add(P1, Pt)
            else:
                Q = P2.diff_add(P1, P)
            toggle = not toggle
            if bit == "1":
                P2 = P2.hybrid_double()
                P1 = Q
            else:
                P1 = P1.hybrid_double()
                P2 = Q

        # correct back at the end
        if bit=="1":
            P2=P2.translate()
            if not toggle:
                P1=P1.translate()
        else:
            P1=P1.translate()
            if not toggle:
                P2=P2.translate()
        return P1, P2

    def hybrid_ladder(self, n):
        P1, P2 = self.full_hybrid_ladder(n)
        return P1

    def to_twisted(self, cls=None):
        return self.parent().to_twistedhtheta(cls=cls)(self)

    def to_twisted_tr(self, cls=None):
        return self.parent().translated_to_twistedhtheta(cls=cls)(self)

    def precomp_ladder(self, n):
        P=self.to_twisted()
        nP=P.precomp_ladder(n)
        return nP.to_montgomery2()
