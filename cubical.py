from sage.all import Integer

class CubicalPoint:
    def __init__(self, P, sigma = 1):
        self._P=P
        # if sigma == 0:
        #     print(f"Warning: sigma=0 for P={P}")
        self._sigma=sigma

    def __repr__(self):
        return f"CubicalPoint {self.XYZ()}"

    def __eq__(self, Q):
        return self.P() == Q.P() and self.sigma() == Q.sigma()

    def P(self):
        return self._P
    def sigma(self):
        return self._sigma
    def data(self):
        return self.P(), self.sigma()
    def curve(self):
        return self.P().curve()
    def zero(self):
        return self.curve()(0)


    # NB: Z and sigma are kinda redundant here
    # but we use the fact that Sage normalize the elliptic points to Z=1
    def XYZ(self):
        X, Y, Z = self.P()
        sigma = self.sigma()
        return (sigma*X, sigma*Y, sigma*Z)
    # in Γ(3(0_E)), X=X2 Z1, Y=Y3, Z=Z1^3, Z1=σ
    def to_elliptic(self):
        X, Y, Z = self.P()
        sigma = self.sigma()
        return (sigma**3*X, sigma**3*Y, sigma**3*Z)
    def X(self):
        X, Y, Z = self.XYZ()
        return X
    def Y(self):
        X, Y, Z = self.XYZ()
        return Y
    def Z(self):
        X, Y, Z = self.XYZ()
        return Z

    def XZ(self):
        X, Y, Z = self.P()
        sigma = self.sigma()
        if self.P().is_zero():
            # warning: σ=0 so this does not work...
            return (sigma*Y, Z)
        else:
            return (sigma*X, sigma*Z)
    # in Γ(2(0_E)), Z=Z1^2, X=X2
    def to_kummer(self, target=None):
        X, Y, Z = self.P()
        sigma = self.sigma()
        if self.P().is_zero():
            # warning: σ=0 so this does not work...
            P=(sigma**2*Y, Z)
        else:
            P=(sigma**2*X, sigma**2*Z)
        if target is None:
            return P
        else:
            return target(P)

    def x(self):
        X, Y, Z = self.P()
        return X/Z
    def y(self):
        X, Y, Z = self.P()
        return Y/Z

    def minus(self):
        return self.__class__(- self.P(), - self.sigma())
    def __neg__(self):
        return self.minus()

    def double(self):
        P, s = self.data()
        return self.__class__(P+P, sigma = 2*s**4 * P[1])

    def diff_add(self, Qs, PmQs):
        P, sP = self.data()
        Q, sQ = Qs.data()
        PmQ, sPmQ = PmQs.data()
        s=sP**2 * sQ**2 * (Q[0]-P[0]) / sPmQ
        return self.__class__(P+Q, sigma = s)

    def three_way_add(self, Q, R, QR, PR, PQ):
        P=self
        P0=P.P()
        Q0=Q.P()
        R0=R.P()
        l = P0._line_(Q0, R0)
        d1 = R0[0]-P0[0]
        d2 = R0[0]-Q0[0]
        r=l/(d1*d2)
        PQR0=P0+Q0+R0
        sigma = r * (QR.sigma() * PR.sigma() * PQ.sigma()) / \
                    (P.sigma()*Q.sigma()*R.sigma())
        return self.__class__(PQR0, sigma)

    #nb: this is like full_ladder3_bis in kummer and biextension
    def full_ladder3(self, n, Q, PQ):
        if n == 0:
            return self.zero(), P
        if n == 1:
            return Q, PQ
        if n < 0:
            PmQ = self.diff_add(Q, PQ)
            return self.ladder3(abs(n), Q, PmQ)
        P=self
        nQ=Q
        nQQ=Q.double()
        PnQQ=PQ.diff_add(Q, P)
        for bit in bin(n-1)[3:]:
            R = nQQ.diff_add(nQ, Q)
            if bit == "0":
                PnQQ=PnQQ.diff_add(nQ, PQ)
                nQ=nQ.double()
                nQQ=R
            else:
                PnQQ=PnQQ.diff_add(nQQ, P)
                nQQ=nQQ.double()
                nQ=R
        return (nQQ, PnQQ)

    def ladder3(self, n, Q, PQ):
        nQ, nQP=self.full_ladder3(n, Q, PQ)
        return nQP

    # returns (n-1)Q, nQ
    # warning: this is not like the other ladder, but more like ladder_bis
    def full_ladder(self, n):
        if n == 0:
            return (self.minus(), self.zero())
        if n == 1:
            return (self.zero(), self)
        if n < 0:
            return self.minus().full_ladder(abs(n))

        Q=self
        nQ=self
        nQQ=self.double()
        for bit in bin(n-1)[3:]:
            R = nQQ.diff_add(nQ, Q)
            if bit == "0":
                nQ=nQ.double()
                nQQ=R
            else:
                nQQ=nQQ.double()
                nQ=R
        return (nQ, nQQ)

    def ratio(self, other, check=True):
        P,sigmaP=self.data()
        Q,sigmaQ=other.data()
        if check:
            assert P==Q
        return (sigmaQ/sigmaP)

    def ladder(self, n):
        P1, P2 = self.full_ladder(n)
        return P2

    def __mul__(self, n):
        if not isinstance(n, (int, Integer)):
            try:
                n = Integer(n)
            except:
                raise TypeError(f"Cannot coerce input scalar {n = } to an integer")
        if not n:
            return self.zero()
        return self.ladder(n)

    def __rmul__(self, n):
        return self * n

    # general method:
    # since σ(0)=0, we need to translate by a random point
    # so that we compute σ(P+T+nQ)/σ(T+nQ) rather than σ(P+nQ)/σ(nQ)
    # or rather σ(P+T+nQ)σ(T)/σ(T+nQ)σ(P+T)
    # but we normalize by σ(T)=σ(P+T)=σ(P+Q)=1
    # this works
    @classmethod
    def non_reduced_tate_T(cls, n, P, Q, T):
        Qs = cls(Q)
        Ts = cls(T)
        PTs = cls(P+T)
        QTs = cls(Q+T)
        PQTs = cls(P+Q+T)
        PTnQs = PTs.ladder3(n, Qs, PQTs)
        TnQs = Ts.ladder3(n, Qs, QTs)
        return TnQs.ratio(PTnQs)

    # other idea: plug T=-Q in the above equation, so we compute
    # σ(P+(n-1)Q)σ(-Q)/σ((n-1)Q)σ(P-Q)
    # if we normalize by σ(P+Q)=σ(Q)=1, then
    # σ(-Q)=-σ(Q) since σ is odd, σ(P-Q)=x(Q)-x(P)
    # This works too!
    @classmethod
    def non_reduced_tate(cls, n, P, Q):
        Qs = cls(Q)
        Ps = cls(P)
        PQs = cls(P+Q)
        nQs, PnQs = Ps.full_ladder3(n-1, Qs, PQs)
        return -(nQs.ratio(PnQs, check=False))/(Q[0]-P[0])

    @classmethod
    def tate(cls, n, P, Q, k=1):
        xP=P[0]
        p=xP.base_ring().characteristic()
        d=(p**k-1)/n
        e = cls.non_reduced_tate(n, P, Q)
        return e**d


if __name__ == "__main__" and "__file__" in globals():
    import time
    from sage.all import ZZ, GF, EllipticCurve, proof

    ea=74
    eb=41
    A = ZZ(2**ea)
    B = ZZ(3**eb)
    p = A * B - 1
    F = GF(p**2, name="i", modulus=[1, 0, 1])
    i = F.gen()

    AC=(7286130371200160433228726316916068871751*i + 624078470490561547110335006058160902979579, 1)
    A24 = ((AC[0]/AC[1]+2)/4)
    E0 = EllipticCurve(F, [0,AC[0]/AC[1],0,1,0])
    n=B
    cofactor = A*B//n

    P = E0.random_point()
    Q = cofactor * E0.random_point()
    T = E0.random_point()

    k=2
    t1=Q.tate_pairing(P, n, k=k)
    t2=CubicalPoint.tate(n,P,Q, k=k)
    #print(t1)
    #print(t2)
    assert t1 == t2

    #tr=tate(n,P,Q, k=k)
