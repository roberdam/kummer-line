from sage.all import cached_method, matrix, vector
from sage.schemes.elliptic_curves.ell_generic import EllipticCurve_generic
from sage.schemes.elliptic_curves.ell_point import EllipticCurvePoint_field
from kummer import KummerLine, KummerPoint, StandardKummerLine
from morphism import Isogeny, TrivialChangeModel, Translation, LinearChangeModel

class ThetaStructure(KummerLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = GenericThetaPoint
        self._type = "GenericTheta"

    @cached_method
    def zero(self):
        return self(self.constants())

    def coords(self):
        return self.zero().coords()

    def to_montgomery(self):
        pass

    def curve(self):
        return self.to_montgomery().codomain().elliptic_curve()

    # lift to an elliptic curve
    def lift_to_elliptic(self, target=None):
        f1=self.to_montgomery()
        f2=f1.codomain().lift_to_elliptic(target=target)
        return f1.compose(f2)

    # the difference with from curve is that we already have the theta constants
    # Now in Kummer: lift_to_elliptic is inversible and we inverse it
    #def reduce_from_elliptic(self, target=None):
    #    f=self.lift_to_elliptic(target=target)
    #    E=f.codomain()
    #    f1=f.first() #conversion from theta to Montgomery
    #    p=StandardKummerLine.to_kummer_line(E)
    #    g=f1.dual()
    #    return p.compose(g)

    def to_kummer_line(self, target=None):
        f1=self.to_montgomery()
        f2=f1.codomain().to_kummer_line(target=target)
        return f1.compose(f2)

class GenericThetaPoint(KummerPoint):
    def double(self):
        return self.diff_add(self, self.zero())

class ThetaLine(ThetaStructure):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = ThetaPoint
        self._type = "Theta"

    @classmethod
    def from_montgomery(cls, E):
        from montgomery import MontgomeryLine
        #if isinstance(E, EllipticCurve_generic):
        E=MontgomeryLine.from_montgomery_curve(E)
        return E.to_theta(cls)

    @classmethod
    def from_curve(cls, E):
        from montgomery import MontgomeryLine
        f=MontgomeryLine.from_curve(E)
        K=f.codomain()
        g=K.to_theta(cls)
        return f.compose(g)

    @classmethod
    def from_four_torsion(cls, T1, T2, TT1=None, TT2=None):
        if isinstance(T1, EllipticCurvePoint_field):
            E=T1.curve()
            TT1=T1+T1
            TT2=T2+T2
            p=StandardKummerLine.to_kummer_line(E)
            K=p.codomain()
            return p.compose(ThetaLine.from_four_torsion(K(T1), K(T2), TT1=K(TT1), TT2=K(TT2)))
        if TT1 is None:
            TT1 = T1.double()
        if TT2 is None:
            TT2 = T2.double()
        x1,z1=T1.coords()
        u1,w1=TT1.coords()
        x2,z2=T2.coords()
        u2,w2=TT2.coords()
        M1 = matrix([[u1*z1/(w1*x1-u1*z1),z1*w1/(w1*x1-u1*z1)],[(-w1*x1**2+2*u1*z1*x1)/(-z1*w1*x1+u1*z1**2),-u1*z1/(w1*x1-u1*z1)]])
        M2 = matrix([[u2*z2/(w2*x2-u2*z2),z2*w2/(w2*x2-u2*z2)],[(-w2*x2**2+2*u2*z2*x2)/(-z2*w2*x2+u2*z2**2),-u2*z2/(w2*x2-u2*z2)]])
        v=vector([1,0])
        theta0 = v+M1*v
        theta1 = M2*theta0
        K = T1.parent()
        N = matrix([[theta0[0], theta0[1]],[theta1[0], theta1[1]]])
        thetanull = N*v
        codomain = cls(thetanull)
        f = LinearChangeModel(domain=K, codomain=codomain, N=N)
        return f

    @cached_method
    #this sends (-a:b)->(0:1), (1:0)->(-1:1), (0:1)->(1:1)
    #(b:a)->(A^2:B^2), (1:1)->(a+b:a-b), (-1:1)->(a-b:a+b)
    def to_montgomery(self, cls=None):
        from montgomery import MontgomeryLine
        if cls is None:
            cls = MontgomeryLine
        a, b = self.coords()
        aa = a**2
        bb = b**2
        T1 = aa + bb
        T2 = aa - bb
        A = -(T1**2 + T2**2) / (T1 * T2)
        codomain = cls((A, 1))
        N = matrix([[b,a],[-b,a]])
        f = LinearChangeModel(domain=self, codomain=codomain, N=N)
        return f

    def to_montgomery2(self, cls=None):
        from montgomery2 import Montgomery2Line
        if cls is None:
            cls = Montgomery2Line
        f=self.to_montgomery()
        N=f.N()
        AA, BB=self.squared_theta()
        codomain = cls((AA, BB))
        return LinearChangeModel(domain=self, codomain=codomain, N=N)

    def to_affine(self):
        return TrivialChangeModel(self, AffineThetaLine(self))

    def squared_theta(self):
        return self.zero().squared_theta()

    @cached_method
    def _precomputation(self):
        a, b = self.coords()
        AA, BB = self.squared_theta()
        return b, a, BB, AA

    def T1(self):
        a,b=self.coords()
        return self((-a,b))
    def T2(self):
        a,b=self.coords()
        return self((b,a))
    def T3(self):
        a,b=self.coords()
        return self((-b,a))
    def TT1(self):
        R=self.base_ring()
        return self((R(1),R(0)))
    def TT1bis(self):
        R=self.base_ring()
        return self((R(0),R(1)))
    def TT2(self):
        R=self.base_ring()
        return self((R(1),R(1)))
    def TT2bis(self):
        R=self.base_ring()
        return self((R(-1),R(1)))

    def translate_T1(self):
        N=matrix([[-1, 0], [0, 1]])
        return Translation(domain=self, N=N)
    def translate_T2(self):
        N=matrix([[0, 1], [1, 0]])
        return Translation(domain=self, N=N)
    def translate_T3(self):
        N=matrix([[0, 1], [-1, 0]])
        return Translation(domain=self, N=N)

    def to_dual(self):
        a,b = self.coords()
        a2 = a+b
        b2 = a-b
        N = matrix([[1, 1],[1, -1]])
        codomain = ThetaLine((a2,b2))
        f = LinearChangeModel(domain=self, codomain=codomain, N=N)
        return f

    # return the 2-isogeny with kernel T=(a: -b)
    # for the isogeny with kernel (b: a), apply Hadamard first
    # if Tb is provided, assume it is a point of 8-torsion above T.
    # If Tb is above the 4-torsion point (1:0), the choice of sign ensure
    # that f(Tb) is (1:0)
    def isogeny_T1(self, Tb=None):
        if Tb is None:
            a, b = self.coords()
            aa = a**2
            bb = b**2
            AA = aa+bb
            BB = aa-bb
            A = AA
            B = (AA*BB).sqrt()
        else:
            TT=Tb.double().double()
            a, b = self.coords()
            assert self((-a, b)) == TT
            r, s = Tb.coords()
            A = r*r+s*s
            B = r*r-s*s
        anew = A+B
        bnew = A-B
        codomain = ThetaLine((anew, bnew))
        def image(P):
            #Cost: 2S+2m0+4a
            x, z = P
            a2, b2 = codomain.coords()
            A = a2+b2
            B = a2-b2
            X = (x*x+z*z)/A
            Z = (x*x-z*z)/B
            return (X+Z, X-Z)
        return Isogeny(domain=self, codomain=codomain, image=image)

    # like isogeny_T1, except we go from θ coordinates to θ' coordinates
    # so composing dual_isogeny_T1 twice gives us [2]
    def dual_isogeny_T1(self, fzero=None):
        if fzero is None:
            a, b = self.coords()
            aa = a**2
            bb = b**2
            AA = aa+bb
            BB = aa-bb
            A = AA
            B = (AA*BB).sqrt()
        else:
            A,B=fzero
        codomain = ThetaLine((A, B))
        def image(P):
            #Cost: 2S+2m0+4a
            x, z = P
            A, B = codomain.coords()
            X = (x*x+z*z)*B
            Z = (x*x-z*z)*A
            return (X, Z)
        return Isogeny(domain=self, codomain=codomain, image=image)

    def isogeny_T2(self, Tb=None):
        dual = self.to_dual()
        if not Tb is None:
            Tb = dual(Tb)
        f = dual.codomain().isogeny_T1(Tb=Tb)
        dual2 = f.codomain().to_dual()
        return dual.compose(f).compose(dual2)

    def radical_isogeny_T1(self):
        return self.isogeny_T1()
    def radical_isogeny_T2(self):
        return self.isogeny_T2()

class ThetaPoint(GenericThetaPoint):
    @cached_method
    def to_montgomery(self, cls=None):
        return self.parent().to_montgomery(cls=cls)(self)
    def to_montgomery2(self, cls=None):
        return self.parent().to_montgomery2(cls=cls)(self)

    @cached_method
    def hadamard(self):
        x,z=self.coords()
        return KummerPoint.to_hadamard(x,z)

    @cached_method
    def squared_theta(self):
        x, z=self.coords()
        return KummerPoint.to_hadamard(x * x, z * z)

    # Cost: 4S+4m0 [2m0 if constants normalised]
    def double(self):
        x0, z0, X0, Z0 = self.parent()._precomputation()
        xp, zp = self.squared_theta()
        xp = X0 * xp**2
        zp = Z0 * zp**2
        X, Z = KummerPoint.to_hadamard(xp, zp)
        X = x0 * X
        Z = z0 * Z
        return self.parent()((X, Z))

    # Cost: 4S+2M+2m+2m0 [1m0 if constants are normalised; 1m if P-Q normalised, and 2S are shared with the double]
    def diff_add(self, Q, PmQ):
        X0, Z0 = self.parent()._precomputation()[-2:]

        p1, p2 = self.squared_theta()
        q1, q2 = Q.squared_theta()
        xp = X0 * p1 * q1
        zp = Z0 * p2 * q2
        PQx, PQz = PmQ.coords()
        X, Z = KummerPoint.to_hadamard(xp, zp)
        X = X * PQz
        Z = Z * PQx

        return self.parent()((X, Z))

    # from f(P), f(Q) and P-Q return P+Q
    # here f is isogeny_T1 to fix ideas
    def half_diff_add(self, fQ, PmQ):
        xP,zP=self
        # invert the hadamard we do in isogeny_T1
        # of course in an optimised implementation we would just not do the
        # hadmard...
        xQ,zQ=fQ
        xPmQ,zPmQ=PmQ
        a=xP*xQ+zP*zQ
        b=xP*xQ-zP*zQ
        return PmQ.parent()((a*zPmQ,b*xPmQ))

    def half_double(self, zero):
        return self.half_diff_add(self, zero)

    # time/memory trade off for the ladder
    def precomp_ladder(self, n):
        if n<0:
            return self.precomp_ladder(abs(n))
        if n==0:
            return self.zero()
        P=self
        # precomputation
        r=[P]
        f=self.parent().dual_isogeny_T1()
        codom=f.codomain()
        g=codom.dual_isogeny_T1(fzero=self.zero())
        for i in range(0, len(bin(n)[2:])):
            phi=f
            if i%2 == 1:
                phi=g
            P=phi(P) #2S+1m0
            r.append(P)
        nnfP=r[-1]
        nfP=nnfP.zero()
        # ladder computation
        for i in range(0, len(r)-1):
            PmQ=r[-i-2]
            zero=PmQ.zero()
            t=nnfP.half_diff_add(nfP, PmQ) #4M; 3M if precomp points are normalised
            b=bin(n)[2+i]
            if b=="1":
                nnfP=nnfP.half_double(zero) #2S+1m0
                nfP=t
            else:
                nfP=nfP.half_double(zero) #2S+1m0
                nnfP=t
        return nfP

class AffineThetaLine(ThetaLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = AffineThetaPoint
        self._type = "AffineTheta"

    @cached_method
    def _precomputation(self):
        a, b = self.coords()
        AA, BB = self.squared_theta()
        return 1/a, 1/b, 1/AA, 1/BB

    def to_projective(self):
        return TrivialChangeModel(self, ThetaLine(self))

class AffineThetaPoint(ThetaPoint):
    def diff_add(self, Q, PmQ):
        X0, Z0 = self.parent()._precomputation()[-2:]

        p1, p2 = self.squared_theta()
        q1, q2 = Q.squared_theta()
        xp = X0 * p1 * q1
        zp = Z0 * p2 * q2
        PQx, PQz = PmQ.coords()
        X, Z = KummerPoint.to_hadamard(xp, zp)
        X = X / PQx
        Z = Z / PQz

        return self.parent()((X/2, Z/2)) #/2 because HoH=2

    def double(self):
        x0, z0, X0, Z0 = self.parent()._precomputation()
        xp, zp = self.squared_theta()
        xp = X0 * xp**2
        zp = Z0 * zp**2
        X, Z = KummerPoint.to_hadamard(xp, zp)
        X = x0 * X
        Z = z0 * Z
        return self.parent()((X/2, Z/2))

# Note in twistedhtheta, 0=(A^2:B^2), T1=(B^2:A^2), T2=(1:0), T3=(0:1)
# this corresponds to    T2,          T3,           0,        T1
# in Montgomery2
class TwistedHThetaLine(ThetaStructure):
    @classmethod
    def from_curve(cls, E):
        from montgomery2 import Montgomery2Line
        f=Montgomery2Line.from_curve(E)
        K=f.codomain()
        g=K.to_twistedhtheta(cls)
        return f.compose(g)

    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = TwistedHThetaPoint
        self._type = "Twisted'Theta"

    def T1(self):
        AA,BB=self.coords()
        return self((BB,AA))
    def T2(self):
        R=self.base_ring()
        return self((R(1),R(0)))
    def T3(self):
        R=self.base_ring()
        return self((R(0),R(1)))
    def TT1(self):
        return self((R(1),R(1)))
    def TT1bis(self):
        R=self.base_ring()
        return self((-R(1),R(1)))

    def translate_T1(self):
        N=matrix([[0, 1], [1, 0]])
        return Translation(domain=self, N=N)

    # same as in Montgomery2
    def translate_T2(self):
        A,B = self.constants()
        N = matrix([[A,-B],[B,-A]])
        return Translation(self, N=N)
    def translate_T3(self):
        B,A = self.constants()
        N = matrix([[A,-B],[B,-A]])
        return Translation(self, N=N)

    def translate(self):
        return self.translate_T2()

    def to_montgomery(self, cls=None):
        from montgomery import MontgomeryLine
        if cls is None:
            cls = MontgomeryLine
        f=self.translate_T2()
        N=f.N()
        AA,BB=self.constants()
        A = -(AA**2 + BB**2) / (AA * BB)
        codomain=cls((A,1))
        return LinearChangeModel(domain=self, codomain=codomain, N=N)

    # Theta to TwistedHTheta is
    # (x:z)->(ax+bz:ax-bz); (a:b)->(A^2:B^2)
    # TwistedHTheta to Montgomery2 is
    # (X:Z)->(A^2X-B^2Z:B^2X-A^2Z)
    # The composition is (x:z)->(bx+az:-bx+az) like in Theta.to_montgomery
    def to_montgomery2(self, cls=None):
        from montgomery2 import Montgomery2Line
        if cls is None:
            cls=Montgomery2Line
        f=self.translate_T2()
        N=f.N()
        codomain=cls(self.constants())
        return LinearChangeModel(domain=self, codomain=codomain, N=N)

    def translated_to_montgomery2(self, cls=None):
        from montgomery2 import Montgomery2Line
        if cls is None:
            cls=Montgomery2Line
        codomain=cls(self.constants())
        return TrivialChangeModel(domain=self, codomain=codomain)

    def isogeny_T2(self, Tb):
        ab, bb = Tb.coords()
        a=ab+bb; b=ab-bb
        codomain = self.__class__((ab**2, bb**2))
        def image(P):
            x, z = P
            # can be computed in 2m0+2S+4a
            X = ((x+z)/a+(x-z)/b)**2
            Z = ((x+z)/a-(x-z)/b)**2
            return (X, Z)
        return Isogeny(domain=self, codomain=codomain, image=image)

    def radical_isogeny_T2(self, sign=1):
        AA,BB=self.constants()
        aa=AA+BB; bb=AA-BB
        ab=(aa*bb).sqrt()
        ab *= sign
        abis=aa+ab; bbis=aa-ab
        return self.isogeny_T2(self((abis, bbis)))

    def isogeny_T1(self):
        AA,BB = self.zero()
        aa=AA+BB
        bb=AA-BB
        codomain = self.__class__((aa, bb))
        def image(P):
            x, z = P
            X = (x+z)**2/aa
            Z = (x-z)**2/bb
            return (X, Z)
        return Isogeny(domain=self, codomain=codomain, image=image)

    def to_affine(self):
        return TrivialChangeModel(self, AffineTwistedHThetaLine(self))

class TwistedHThetaPoint(GenericThetaPoint):
    def diff_add(self, Q, PmQ):
        xP, zP = self.XZ()
        xQ, zQ = Q.XZ()
        xPmQ, zPmQ = PmQ.XZ()
        a, b = self.zero()
        A, B = KummerPoint.to_hadamard(a,b)
        t = (xP+zP)*(xQ+zQ)*B
        u = (xP-zP)*(xQ-zQ)*A
        X = (t+u)*(t+u)*zPmQ
        Z = (t-u)*(t-u)*xPmQ
        return self.parent()((X, Z))

    def to_montgomery2(self, cls=None):
        return self.parent().to_montgomery2(cls=cls)(self)

    def to_montgomery2_tr(self, cls=None):
        return self.parent().translated_to_montgomery2(cls=cls)(self)

    def translate(self):
        return self.translate_T2()

    # time/memory trade off for the ladder
    def precomp_ladder(self, n):
        if n<0:
            return self.precomp_ladder(abs(n))
        if n==0:
            return self.zero()
        xP,zP=self
        xP,zP=xP+zP,xP-zP #conversion from θtw' to θtw

        # precomputation
        r=[(xP,zP)]
        AA,BB=self.zero()
        aa,bb=AA+BB,AA-BB

        for i in range(0, len(bin(n)[2:])):
            #2S+1m0
            if i%2 == 0:
            #isogeny from E1
                s=xP*xP*bb
                t=zP*zP*aa
                xP,zP=s+t,s-t
            else:
            #isogeny from E2
                s=xP*xP*BB
                t=zP*zP*AA
                xP,zP=s+t,s-t
            r.append((xP,zP))
        xP2,zP2=r[-1] #nnP
        R=xP2.parent()
        xP1,zP1=R(1),R(1) #nP
        prev="1"
        # NB: what we could do instead is store
        # bb*xP, aa*zP
        # then the precomputation would be 1M+1S+1m0 instead of 2S+1m0
        # but the ladder computation would not alternate between different
        # ways of twisting the theta coordinates so would be slightly
        # faster

        # ladder computation
        for i in range(0, len(r)-1):
            #Half diff add: 4M
            xPmQ,zPmQ=r[-i-2]
            s=xP1*xP2
            t=zP1*zP2
            s,t=s+t,s-t
            X=s*zPmQ
            Z=t*xPmQ
            b=bin(n)[2+i]
            if b=="1":
                if prev=="0": #P2 was the point with the diff add, hence twisted by (b_i^2:a_i^2)
                    if (len(r)-i+1)%2==0:
                        zero1=aa
                        zero2=bb
                    else:
                        zero1=AA
                        zero2=BB
                else:
                    if (len(r)-i+1)%2==0:
                        zero1=bb
                        zero2=aa
                    else:
                        zero1=BB
                        zero2=AA
                # Double: 2S+1m0
                s=xP2**2*zero1
                t=zP2**2*zero2
                xP2,zP2=s+t,s-t
                # Diff Add
                xP1,zP1=X,Z
            else:
                if prev=="0": #P1 was the point with the double, hence twisted by (a_i^2:b_i^2)
                    if (len(r)-i+1)%2==0:
                        zero1=bb
                        zero2=aa
                    else:
                        zero1=BB
                        zero2=AA
                else:
                    if (len(r)-i+1)%2==0:
                        zero1=aa
                        zero2=bb
                    else:
                        zero1=AA
                        zero2=BB
                # Double: 2S+1m0
                s=xP1**2*zero1
                t=zP1**2*zero2
                xP1,zP1=s+t,s-t
                # Diff Add
                xP2,zP2=X,Z
            prev=b

        if prev=="1": #we need to go from the twist by (b:a) to (a:b)
            xP1=aa*xP1
            zP1=bb*zP1
        return self.parent()((xP1+zP1,xP1-zP1))

class AffineTwistedHThetaLine(TwistedHThetaLine):
    def __init__(self, constants, **kwds):
        super().__init__(constants, **kwds)
        self._point = AffineTwistedHThetaPoint
        self._type = "AffineTwisted'Theta"

    def to_projective(self):
        return TrivialChangeModel(self, TwistedHThetaLine(self))

class AffineTwistedHThetaPoint(TwistedHThetaPoint):
    def diff_add(self, Q, PmQ):
        xP, zP = self.XZ()
        xQ, zQ = Q.XZ()
        xPmQ, zPmQ = PmQ.XZ()
        a, b = self.zero()
        A, B = KummerPoint.to_hadamard(a,b)
        t = (xP+zP)*(xQ+zQ)/A
        u = (xP-zP)*(xQ-zQ)/B
        X = (t+u)*(t+u)/xPmQ
        Z = (t-u)*(t-u)/zPmQ
        return self.parent()((X, Z))
