from ellcurve import Curve, CurvePoint
from sage.all import cached_method, Integer, matrix
# from sage.schemes.elliptic_curves.ell_generic import EllipticCurve_generic
from sage.schemes.elliptic_curves.ell_point import EllipticCurvePoint_field
from morphism import ChangeModel, LinearChangeModel, Isomorphism
# from sage.structure.element import get_coercion_model, RingElement
# cm = get_coercion_model()

class ProjUtils:
    @staticmethod
    def proj_equal(P, Q):
        x1, z1 = P
        x2, z2 = Q
        assert (x1 != 0 or z1 != 0)
        assert (x2 != 0 or z2 != 0)
        return (x1 * z2 == x2 * z1)

    @staticmethod
    def affine_equal(P, Q):
        x1, z1 = P
        x2, z2 = Q
        return (x1 == x2 and z1 == z2)

class KummerLine(Curve):
    def __init__(self, constants=None, **kwds):
        super().__init__(**kwds)
        self._point = KummerPoint
        self._type = "GenericKummer"
        if not constants is None:
            self._constants = tuple(constants)
        else:
            self._constants = None

    def __repr__(self):
        return f"{self._type} constants={self.constants()} curve={self.curve()}"

    def __getitem__(self, item):
        return self._constants[item]

    def __setitem__(self, key, item):
        self._constants[key] = item

    def constants(self):
        return self._constants

    def coords(self):
        return self.constants()

    @cached_method
    def base_ring(self):
        if self._base_ring is None and self.constants():
            return self.constants()[0].parent()
        return super().base_ring()

    def __eq__(self, other):
        # if self.base_ring() != other.base_ring():
        #     return False
        if self.constants() and other.constants():
            return ProjUtils.proj_equal(self.constants(), other.constants())
        else:
            return self.curve() == other.curve()

    def elliptic_curve(self):
        return self.curve()

    def j_invariant(self):
        return self.elliptic_curve().j_invariant()

    def lift_to_elliptic(self, target=None):
        pass
    def reduce_from_elliptic(self, target=None):
        return self.lift_to_elliptic(target=target).inverse()

    # 2-torsion points
    def T1(self):
        pass
    def T2(self):
        pass
    # T1+T2
    def T3(self):
        pass
    # 4-torsion points
    def TT1(self):
        pass
    def TT1bis(self):
        pass
    def TT2(self):
        pass
    def TT2bis(self):
        pass
    def isogeny_T1(self):
        pass
    def isogeny_T2(self):
        pass
    def translate_T1(self):
        pass
    def translate_T2(self):
        pass
    def translate_T3(self):
        pass
    def translate_by(self, T):
        if T == self.T1():
            return self.translate_T1()
        elif T == self.T2():
            return self.translate_T2()
        elif T == self.T3():
            return self.translate_T3()
        else:
            assert T == self.zero()
            return self

    def random_point(self):
        R=self.base_ring()
        x=R.random_element()
        z=R.random_element()
        return self((x,z))

class KummerPoint(CurvePoint):
    @staticmethod
    def to_hadamard(x, z):
        return x+z, x-z

    def constants():
        return self.parent().constants()

    def XZ(self):
        return self.coords()

    def X(self):
        X, Z = self.coords()
        return X

    def Z(self):
        X, Z = self.coords()
        return Z

    def x(self):
        X, Z = self.coords()
        return X/Z

    def z(self):
        X, Z = self.coords()
        return Z/X

    def is_zero(self):
        return self == self.zero()

    def __bool__(self):
        """
        A point represents False if it is the neutral point and True otherwise
        """
        return bool(self.is_zero())

    def double(self):
        pass

    def diff_add(self, Q, PmQ):
        pass

    def scale(self, n):
        x, z = self.coords()
        scaled_coords = (n * x, n * z)
        return self._parent(scaled_coords)

    # returns mP, (m+1)P
    def full_ladder(self, n):
        n = abs(n)
        P = self
        P1, P2 = P.zero(), P

        if n == 0:
            return P1, P2

        for bit in bin(n)[2:]:
            # on the first bit we add 0, so we could treat this case separately, ie start with `P1, P2=P, P.double()` and use `for bit in bin(n)[3:]`
            Q = P2.diff_add(P1, P)
            if bit == "1":
                P2 = P2.double()
                P1 = Q
            else:
                P1 = P1.double()
                P2 = Q

        return P1, P2

    # returns (m-1)P, mP, and start the ladder from P, 2P;
    # rather than 0, P
    # (yes full_ladder returns mP, (m+1)P, this api sucks I know)
    def full_ladder_bis(self, n):
        n = abs(n)
        P = self

        if n == 0:
            return (self, P.zero())
        if n == 1:
            return (P.zero(), self)

        P1, P2 = P, P.double()
        # if n == 2:
        #     return (P1, P2)

        for bit in bin(n-1)[3:]:
            Q = P2.diff_add(P1, P)
            if bit == "1":
                P2 = P2.double()
                P1 = Q
            else:
                P1 = P1.double()
                P2 = Q

        return P1, P2

    # returns mP
    def ladder(self, n):
        P1, P2 = self.full_ladder(n)
        return P1

    # returns mP
    def ladder_bis(self, n):
        P1, P2 = self.full_ladder_bis(n)
        return P2

    def __mul__(self, n):
        if not isinstance(n, (int, Integer)):
            try:
                n = Integer(n)
            except:
                raise TypeError(f"Cannot coerce input scalar {n = } to an integer")

        if not n:
            return self.parent().zero()

        return self.ladder(n)

    def __rmul__(self, n):
        return self * n

    # we assume we have P-Q; we could instead assume we have P+Q with some minor changes
    def full_ladder3(self, n, Q, PmQ):
        """
        From [self=P], [Q], [P-Q], return [nQ], [P+n Q]
        """
        P = self
        nQ=Q.zero()
        nQQ=Q
        nQP=P

        if n == 0:
            return nQ, nQP

        if n < 0:
            PpQ = P.diff_add(Q, PmQ)
            return self.full_ladder3(abs(n), Q, PpQ)

        # Montgomery-ladder
        for bit in bin(n)[2:]:
            # on the first bit we add 0, so we could treat this case separately
            R = nQQ.diff_add(nQ, Q)
            if bit == "0":
                nQP=nQP.diff_add(nQ, P)
                nQ=nQ.double()
                nQQ=R
            else:
                nQP=nQP.diff_add(nQQ, PmQ)
                nQQ=nQQ.double()
                nQ=R
        return (nQ, nQP)

    # we assume here we have P+Q
    def full_ladder3_bis(self, n, Q, PQ):
        """
        From [self=P], [Q], [P+Q], return [nQ], [P+n Q]
        """
        if n == 0:
            return Q.zero(), self
        if n == 1:
            return Q, PQ

        P = self
        nQ=Q
        nQQ=Q.double()
        nQQP=PQ.diff_add(Q, P)

        # if n == 2:
        #     return nQQ, nQQP

        if n < 0:
            PmQ = P.diff_add(Q, PQ)
            return self.full_ladder3_bis(abs(n), Q, PmQ)

        for bit in bin(n-1)[3:]:
            R = nQQ.diff_add(nQ, Q)
            if bit == "0":
                nQQP=nQQP.diff_add(nQ, PQ)
                nQ=nQ.double()
                nQQ=R
            else:
                nQQP=nQQP.diff_add(nQQ, P)
                nQQ=nQQ.double()
                nQ=R
        return (nQQ, nQQP)

    def ladder3(self, n, Q, PmQ):
        nQ, nQP=self.full_ladder3(n, Q, PmQ)
        return nQP

    def proj_equal(self, other):
        x1, z1 = self
        x2, z2 = other
        return ProjUtils.proj_equal((x1, z1), (x2, z2))

    def affine_equal(self, other):
        x1, z1 = self
        x2, z2 = other
        return ProjUtils.affine_equal((x1, z1), (x2, z2))

    def __eq__(self, other):
        """ """
        if not isinstance(other, KummerPoint):
            return False
        if self._parent != other._parent:
            return False
        return self.proj_equal(other)

    def equal(self, other):
        """ Affine equality test """
        if not isinstance(other, KummerPoint):
            return False
        if self._parent != other._parent:
            return False
        return self.affine_equal(other)

    # give the ratio to go from P to Q, ie λP=Q
    def ratio(self, Q):
        XP, ZP = self.coords()
        XQ, ZQ = Q.coords()
        if XP == 0:
            assert XQ == 0
            return (ZQ/ZP)
        else:
            l=XQ/XP
            assert (ZQ == l*ZP)
            return l

    # we have P=self, P+T, Q, Q+T and we compute P+Q, P+Q+T
    def compatible_add(self, PT, Q, QT):
        pass

    # we have P=self, Q, R, Q+R, P+R, P+Q and we compute P+Q+R
    def three_way_add(self, Q, R, QR, PR, PQ):
        pass


    def translate_T1(self):
        f = self.parent().translate_T1()
        return f(self)

    def translate_T2(self):
        f = self.parent().translate_T2()
        return f(self)

    def translate_T3(self):
        f = self.parent().translate_T3()
        return f(self)

    def translate_by(self, T):
        f = self.parent().translate_by(T)
        return f(self)

    def isogeny_T1(self):
        f = self.parent().isogeny_T1()
        return f(self)

    def isogeny_T2(self):
        f = self.parent().isogeny_T2()
        return f(self)

    def to_projective(self):
        f = self.parent().to_projective()
        return f(self)

    def to_affine(self):
        f = self.parent().to_affine()
        return f(self)

    def lift_to_elliptic_point(self, target=None):
        return self.parent().lift_to_elliptic(target=target)(self)

class StandardKummerLine(KummerLine):
    @classmethod
    def from_curve(cls, E):
        return cls.to_kummer_line(E)

    @classmethod
    def to_kummer_line(cls,E):
        K=cls.kummer_line(E)
        def inv(P):
            return P.lift_to_elliptic_point(E)
        return ChangeModel(domain=E, codomain=K, image=K, inverse_image=inv)

    @classmethod
    def kummer_line(cls, E):
        return cls(E)

    def __init__(self, curve=None, **kwds):
        super().__init__(curve=curve, **kwds)
        self._point = StandardKummerPoint
        self._type = "Elliptic Kummer Line"

    def zero(self):
        R=self.base_ring()
        return self((R(1), R(0)))

    def lift_to_elliptic(self, target=None):
        """
        Deterministically lift an x-coordinate taking the smallest y-coordinate as the chosen root.
        """
        E = self.curve()
        #print(f"Lift to elliptic: {E}")
        a2 = E.a2()
        a4 = E.a4()
        a6 = E.a6()
        assert E.a1()==0 and E.a3()==0
        def image(P):
            if P.is_zero():
                return E(0)
            # Compute y2, assume x is a valid coordinate
            x = P.x()
            y2 = x**3 + a2*x**2 + a4*x + a6
            y = y2.sqrt()
            return E(x, y)
        phi=ChangeModel(domain=self, codomain=E, image=image, inverse_image= self)
        if not target is None:
            psi=Isomorphism.isomorphism(E,target)
            return phi.compose(psi)
        return phi

    def isomorphism_to(self, K2):
        f=self.curve().isomorphism_to(K2.curve())
        u=f.u
        r=f.r
        N = matrix([[1,-r],[0,u**2]])
        return LinearChangeModel(domain=self, codomain=K2, N=N)

class StandardKummerPoint(KummerPoint):
    def __init__(self, parent, coords):
        if isinstance(coords, EllipticCurvePoint_field):
            if coords.is_zero():
                zero=coords[2] #0
                R=zero.parent()
                coords = [R(1), zero]
            else:
                coords = [coords[0], coords[2]]
        super().__init__(parent, coords)

    def double(self):
        P=self.lift_to_elliptic_point()
        return self.parent()(P+P)

    def diff_add(self, Q, PmQ):
        K=self.parent()
        P=self.lift_to_elliptic_point()
        QQ=Q.lift_to_elliptic_point()
        if K(P-QQ) != PmQ:
            QQ=-QQ
        return K(P+QQ)
